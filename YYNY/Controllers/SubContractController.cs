﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YYNY.Tools;
using YYNY.Tools.Dao;
using System.IO;
using Newtonsoft.Json.Linq;

namespace YYNY.Controllers
{
    public class SubContractController : Controller
    {
        BaseDao dao = new BaseDao();
        // GET: SubContract
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SubConInfoView()
        {
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;
            return View();
        }

        /// <summary>
        /// 新建分包合同
        /// </summary>
        /// <returns></returns>
        public ActionResult SubConInfoAddView()
        {
            ViewBag.SubContractId = "";
            ViewBag.ProjectId = "";
            ViewBag.HTMC = "";
            ViewBag.HTLX = "";
            ViewBag.DFDWMC = "";
            ViewBag.HTJE = "";
            ViewBag.JSFS = "";
            ViewBag.ZFFS = "";

            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;
            return View("SubConInfoEditView");
        }

        public ActionResult SubConInfoEditView()
        {

            string SubContractId = Request.QueryString["SubContractId"] == null ? null : Request.QueryString["SubContractId"].ToString().Replace(" ", ""); ;
            string sql = "select * from t_d_SubContract where SubContractId=" + SubContractId;
            Dictionary<string,object> map = dao.GetList(sql, new string[] { "ProjectId", "HTMC", "HTLX", "DFDWMC", "HTJE", "JSFS", "ZFFS" })[0];
            ViewBag.SubContractId = SubContractId;
            ViewBag.ProjectId = map["ProjectId"].ToString();
            ViewBag.HTMC = map["HTMC"].ToString();
            ViewBag.HTLX = map["HTLX"].ToString();
            ViewBag.DFDWMC = map["DFDWMC"].ToString();
            ViewBag.HTJE = map["HTJE"].ToString();
            ViewBag.JSFS = map["JSFS"].ToString();
            ViewBag.ZFFS = map["ZFFS"].ToString();

            sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;
            return View("SubConInfoEditView");
        }

        public ActionResult SubConBriefView()
        {
            string SubContractId = Request.QueryString["SubContractId"] == null ? null : Request.QueryString["SubContractId"].ToString();
            string sql = "select a.*,b.Name from t_d_SubContract a inner join t_d_ProjectInfo b on a.projectid=b.projectid where SubContractId=" + SubContractId;
            Dictionary<string, object> map  = dao.GetList(sql, new string[] { "Name","ProjectId", "HTMC", "HTLX", "DFDWMC", "HTJE", "JSFS", "ZFFS" })[0];

            try
            {
                ViewBag.Name = map["Name"].ToString();
                ViewBag.HTMC = map["HTMC"].ToString();
                ViewBag.HTLX = map["HTLX"].ToString();
                ViewBag.DFDWMC = map["DFDWMC"].ToString();
                ViewBag.HTJE = map["HTJE"].ToString();
                ViewBag.JSFS = map["JSFS"].ToString();
                ViewBag.ZFFS = map["ZFFS"].ToString();

                return View("SubConBriefView");
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /ProjectController/InfoTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }
        }


        public ActionResult InfoTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,a.SubContractId,b.ProjectId,b.Name,a.HTMC,a.HTLX,a.DFDWMC,a.HTJE,a.JSFS,a.ZFFS" +
                        "" +
                        " FROM t_d_SubContract a inner join t_d_ProjectInfo b on a.ProjectId=b.ProjectId " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "SubContractId", "ProjectId", "Name", "HTMC", "HTLX", "DFDWMC", "HTJE", "JSFS", "ZFFS" });
                SqlNum = "select count(SubContractId) as TotalNum  FROM t_d_SubContract a inner join t_d_ProjectInfo b on a.ProjectId=b.ProjectId where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /SubContractController/InfoTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        public ActionResult ConAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string ProjectId = obj["ProjectId"] == null ? null : obj["ProjectId"].ToString().Replace(" ", "");
            string HTMC = obj["HTMC"] == null ? null : obj["HTMC"].ToString().Replace(" ", "");
            string HTLX = obj["HTLX"] == null ? null : obj["HTLX"].ToString().Replace(" ", "");
            string DFDWMC = obj["DFDWMC"] == null ? null : obj["DFDWMC"].ToString().Replace(" ", "");
            string HTJE = obj["HTJE"] == null ? null : obj["HTJE"].ToString().Replace(" ", "");
            string JSFS = obj["JSFS"] == null ? null : obj["JSFS"].ToString().Replace(" ", "");
            string ZFFS = obj["ZFFS"] == null ? null : obj["ZFFS"].ToString().Replace(" ", "");


            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["ProjectId"] = ProjectId,
                    ["HTMC"] = HTMC,
                    ["HTLX"] = HTLX,
                    ["DFDWMC"] = DFDWMC,
                    ["HTJE"] = HTJE,
                    ["JSFS"] = JSFS,
                    ["ZFFS"] = ZFFS,
                    ["IsDelete"] = 0,
                };
                dao.save("t_d_SubContract", map);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult ConUpdate()
        {
                        var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string SubContractId = obj["SubContractId"] == null ? null : obj["SubContractId"].ToString().Replace(" ", "");
            string ProjectId = obj["ProjectId"] == null ? null : obj["ProjectId"].ToString().Replace(" ", "");
            string HTMC = obj["HTMC"] == null ? null : obj["HTMC"].ToString().Replace(" ", "");
            string HTLX = obj["HTLX"] == null ? null : obj["HTLX"].ToString().Replace(" ", "");
            string DFDWMC = obj["DFDWMC"] == null ? null : obj["DFDWMC"].ToString().Replace(" ", "");
            string HTJE = obj["HTJE"] == null ? null : obj["HTJE"].ToString().Replace(" ", "");
            string JSFS = obj["JSFS"] == null ? null : obj["JSFS"].ToString().Replace(" ", "");
            string ZFFS = obj["ZFFS"] == null ? null : obj["ZFFS"].ToString().Replace(" ", "");


            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["ProjectId"] = ProjectId,
                    ["HTMC"] = HTMC,
                    ["HTLX"] = HTLX,
                    ["DFDWMC"] = DFDWMC,
                    ["HTJE"] = HTJE,
                    ["JSFS"] = JSFS,
                    ["ZFFS"] = ZFFS,
                    ["IsDelete"] = 0,
                };
                dao.update("t_d_SubContract", map, "SubContractId="+ SubContractId);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult SubConInfoDel()
        {
            string SubContractId = Request.Form["SubContractId"] == null ? null : Request.Form["SubContractId"].ToString().Replace(" ", "");
            string sql = "update t_d_SubContract set IsDelete=1 where SubContractId=" + SubContractId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult Upload()
        {


            string tpye = Request.Files[0].ContentType;
            string UpName = Request.Files[0].FileName;
            //获取后缀名
            string exName = UpName.Substring(UpName.LastIndexOf(".") + 1, (UpName.Length - UpName.LastIndexOf(".") - 1));
            string fileName = "Purchase" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + exName;
            if (Directory.Exists(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd"))) == false)//如果不存在就创建file文件夹
            {
                Directory.CreateDirectory(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd")));
            }
            string filePhysicalPath = Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd") + "/" + fileName);

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                if (Request.Form["ProjectId"] == "")
                {
                    result.Add("result", "没有选择主项目");
                    return Json(result);
                }

                Request.Files[0].SaveAs(filePhysicalPath);

                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["ProjectId"] = Request.Form["ProjectId"],
                    ["Name"] = UpName,
                    ["Path"] = "/upload/" + DateTime.Now.ToString("yyyyMMdd") + "/" + fileName,
                    ["TypeName"] = "SubContract",
                    ["IsDelete"] = 0
                };
                dao.save("t_d_File", map);

                result.Add("result", "OK");
                return Json(result);
                //upImg.SaveAs(filePhysicalPath);
                //pic = "/upload/" + fileName;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

    }
}