﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YYNY.Tools.Dao;
using System.IO;

namespace YYNY.Controllers
{
    public class HomeController : Controller
    {
        BaseDao dao = new BaseDao();
        public ActionResult Index()
        {
            //获取cookies
            if (Request.Cookies["UserID"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            string Sql = "select UserName from t_d_user where UserID='" + Request.Cookies["UserID"].Value + "'";
            List<Dictionary<string, object>> UserList = new List<Dictionary<string, object>>();
            UserList = dao.GetList(Sql, new string[] { "UserName" });
            string UserName = UserList[0]["UserName"].ToString();
            ViewBag.UserName = UserName;

            //Sql = "select UserId as TotalNum FROM t_d_user where UserId=10";
            BaseDao.execute(Sql);

            return View();
        }


        public ActionResult Lesson()
        {
            return View();
        }

        public ActionResult console()
        {
            return View();
        }

        public ActionResult DevLog()
        {
            return View("DevLogView");
        }





        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}