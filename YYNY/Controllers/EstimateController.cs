﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YYNY.Tools;
using YYNY.Tools.Dao;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace YYNY.Controllers
{
    public class EstimateController : Controller
    {
        BaseDao dao = new BaseDao();
        // GET: Estimate
        public ActionResult EditIndex()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string ProjectName = Request.Form["ProjectName"] == null ? null : Request.Form["ProjectName"].ToString().Replace(" ", "");
            string ProjectType = Request.Form["ProjectType"] == null ? null : Request.Form["ProjectType"].ToString().Replace(" ", "");
            string PreEstName = Request.Form["PreEstName"] == null ? null : Request.Form["PreEstName"].ToString().Replace(" ", "");

            

            ViewBag.PreEstId = PreEstId;
            ViewBag.ProjectName = ProjectName;
            ViewBag.ProjectType = ProjectType;
            ViewBag.PreEstName = PreEstName;

            return View();
        }


        public ActionResult EstimateUserView()
        {
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1 and Type='用户工程'";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;
            return View();
        }

        public ActionResult EstimateMainlyView()
        {
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1 and Type='主业'";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;
            return View();
        }

        public ActionResult EstimateViewTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

                if (condition.Contains("用户工程"))
                {
                    sql = " select top " + limit.ToString() + " * from " +
                            " ( " +
                            " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,PreEstId,Name,ProjectId" +
                            "" +
                            " FROM o_PreEstUser " +
                            " where 1=1 " + condition +
                            " ) as s " +
                            " where rowNumber>" + (limit * (page - 1)).ToString();

                    list = dao.GetList(sql, new string[] { "PreEstId", "Name", "ProjectId" });
                    SqlNum = "select count(PreEstId) as TotalNum FROM o_PreEstUser where 1=1 " + condition;
                }
                else
                {
                    sql = " select top " + limit.ToString() + " * from " +
                            " ( " +
                            " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,PreEstId,Name,ProjectId" +
                            "" +
                            " FROM o_PreEstMainly " +
                            " where 1=1 " + condition +
                            " ) as s " +
                            " where rowNumber>" + (limit * (page - 1)).ToString();

                    list = dao.GetList(sql, new string[] { "PreEstId", "Name", "ProjectId" });
                    SqlNum = "select count(PreEstId) as TotalNum FROM o_PreEstMainly where 1=1 " + condition;
                }

                
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/InstallFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }




        public ActionResult EstUserCSTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,PreEstId,Name,ProjectId" +
                        "" +
                        " FROM o_PreEstUserCS " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                list = dao.GetList(sql, new string[] { "PreEstId", "Name", "ProjectId" });
                SqlNum = "select count(PreEstId) as TotalNum FROM o_PreEstUserCS where 1=1 " + condition;

                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/InstallFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        /// <summary>
        /// 主业前期测算 结算表
        /// </summary>
        /// <returns></returns>
        public ActionResult EstMainlyJSTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,PreEstId,Name,ProjectId" +
                        "" +
                        " FROM o_PreEstMainlyJS " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                list = dao.GetList(sql, new string[] { "PreEstId", "Name", "ProjectId" });
                SqlNum = "select count(PreEstId) as TotalNum FROM o_PreEstMainlyJS where 1=1 " + condition;

                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/InstallFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }



        public ActionResult Add()
        {
            string ProjectType = Request.Form["ProjectType"] == null ? null : Request.Form["ProjectType"].ToString().Replace(" ", "");
            string ProjectId = Request.Form["ProjectId"] == null ? null : Request.Form["ProjectId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                if (ProjectType == "用户工程")
                {
                    Dictionary<string, object> map = new Dictionary<string, object>()
                    {
                        ["Name"] = Name,
                        ["Type"] = ProjectType,
                        ["ProjectId"] = ProjectId,
                        ["CreateTime"] = DateTime.Now.ToString(),
                        ["IsDelete"] = 0,

                        ["JZMJ"] = 0,
                        ["RL"] = 0,
                        ["WXCD"] = 0,
                        ["HXNBJJE"] = 0,
                        ["YZYQ_CF"] = 0,
                        ["YZYQ_YHYB"] = 0,
                        ["YZYQ_LLLJ"] = 0,
                        ["YZYQ_CDZ"] = 0,
                        ["DJHTJE"] = 0,
                        ["SJF"] = 0,
                        ["JLF"] = 0,
                        ["DJGLF"] = 0,
                        ["SJ"] = 0,
                        ["ZJCB"] = 0,
                        ["GSGLF"] = 0,
                        ["LRL"] = 0,

                    };
                    dao.save("o_PreEstUser", map);

                }
                else if(ProjectType=="主业")
                {
                    //新建主业工程
                    Dictionary<string, object> map = new Dictionary<string, object>()
                    {
                        ["Name"] = Name,
                        ["Type"] = ProjectType,
                        ["ProjectId"] = ProjectId,
                        ["CreateTime"] = DateTime.Now.ToString(),
                        ["IsDelete"] = 0,

                        ["TZZE"] = 0,
                        ["SJF"] = 0,
                        ["JLF"] = 0,
                        ["JHTJSGF"] = 0,
                        ["JHAZSGF"] = 0,
                        ["JHZC"] = 0,
                        ["JHSYF"] = 0,
                        ["JHPCF"] = 0,
                        ["CBZC"] = 0,
                        ["CBBJSGF"] = 0,
                        ["CBPCF"] = 0,
                        ["JHBJSGF"] = 0,
                        ["CBSJ"] = 0,
                        ["CBGSGLF"] = 0,
                    };
                    dao.save("o_PreEstMainly", map);
                }
                else if(ProjectType == "主业结算")
                {
                    Dictionary<string, object> map = new Dictionary<string, object>()
                    {
                        ["Name"] = Name,
                        ["Type"] = ProjectType,
                        ["ProjectId"] = ProjectId,
                        ["CreateTime"] = DateTime.Now.ToString(),
                        ["IsDelete"] = 0,

                        ["TZZE"] = 0,
                        ["SJF"] = 0,
                        ["JLF"] = 0,
                        ["JHTJSGF"] = 0,
                        ["JHAZSGF"] = 0,
                        ["JHZC"] = 0,
                        ["JHSYF"] = 0,
                        ["JHPCF"] = 0,
                        ["CBZC"] = 0,
                        ["CBBJSGF"] = 0,
                        ["CBPCF"] = 0,
                        ["JHBJSGF"] = 0,
                        ["CBSJ"] = 0,
                        ["CBGSGLF"] = 0,
                    };
                    dao.save("o_PreEstMainlyJS", map);
                }



                //BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        /// <summary>
        /// 主业前期测算表 删除
        /// </summary>
        /// <returns></returns>
        public ActionResult EstMainlyDel()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string sql = "update o_PreEstMainly set IsDelete=1 where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        /// <summary>
        /// 主业结算表 删除
        /// </summary>
        /// <returns></returns>
        public ActionResult EstMainlyJSDel()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string sql = "update o_PreEstMainlyJS set IsDelete=1 where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }



        public ActionResult AddEstUserCS()
        {
            string PreEstUserId = Request.Form["PreEstUserId"] == null ? null : Request.Form["PreEstUserId"].ToString().Replace(" ", "");
            string ProjectId = Request.Form["ProjectId"] == null ? null : Request.Form["ProjectId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
                string sql = "";
                //Dictionary<string, object> map2 = new Dictionary<string, object>();
                //string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                //"Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                //    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId" };

                //sql = "select * from o_PreEstUser where PreEstId=" + PreEstUserId;
                //Dictionary<string, object> map = new Dictionary<string, object>();
                //string[] field2 = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                //"Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                //    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL" };
                //map = dao.GetList(sql, field2)[0];

                //map.Add("HTJE", HTJE);
                //map.Add("PreEstUserId", PreEstUserId);

                //map.Add("Name", Name);
                //map.Add("Type", "用户工程前期测算");
                //map.Add("ProjectId", ProjectId);
                //map.Add("CreateTime", DateTime.Now.ToString());
                //map.Add("IsDelete", 0);
                //dao.save("o_PreEstUserCS", map);

                //下面查找合同金额
                sql = "select ConAmount from t_d_ProjectInfo where ProjectId=" + ProjectId;
                string HTJE = dao.GetList(sql, new string[] { "ConAmount" })[0]["ConAmount"].ToString();

                sql = "BEGIN";
                sql += " DECLARE @tempid int,@tempEstId int;";
                sql += " SET @tempEstId=" + PreEstUserId + ";";
                sql += " insert INTO o_PreEstUserCS (Name,ProjectId,CreateTime,IsDelete,Type,XMMC,YDXZ,SSQY,JZMJ,RL,WXCD,HXNBJJE,";
                sql += " Memo_WXTJ,Memo_WXSB,Memo_WXCL,Memo_KBSSB,Memo_KBSCL,Memo_GYSB,Memo_GYCL,Memo_ZYSB,Memo_ZYCL,";
                sql += " Memo_ZTAZ,Memo_ZTTJSGF,Memo_ZTSYTSF,YZYQ_CF,Memo_YZYQCF,YZYQ_YHYB,Memo_YZYQYHYB,YZYQ_LLLJ,Memo_YZYQLLLJ,";
                sql += " YZYQ_CDZ,Memo_YZYQCDZ,DJHTJE,Memo_DJHTJE,SJF,Memo_SJF,JLF,Memo_JLF,DJGLF,Memo_DJGLF,SJ,Memo_SJ,ZJCB,Memo_ZJCB,";
                sql += " GSGLF,Memo_GSGLF,Memo_YWF,LRL,Memo_LRL,HTJE,PreEstUserId,LPYT,GPRL,ZPRL) ";
                sql += " SELECT '" + Name + "',ProjectId,CreateTime,IsDelete,'用户工程前期测算',XMMC,YDXZ,SSQY,JZMJ,RL,WXCD,HXNBJJE,";
                sql += " Memo_WXTJ,Memo_WXSB,Memo_WXCL,Memo_KBSSB,Memo_KBSCL,Memo_GYSB,Memo_GYCL,Memo_ZYSB,Memo_ZYCL,";
                sql += " Memo_ZTAZ,Memo_ZTTJSGF,Memo_ZTSYTSF,YZYQ_CF,Memo_YZYQCF,YZYQ_YHYB,Memo_YZYQYHYB,YZYQ_LLLJ,Memo_YZYQLLLJ,";
                sql += " YZYQ_CDZ,Memo_YZYQCDZ,DJHTJE,Memo_DJHTJE,SJF,Memo_SJF,JLF,Memo_JLF,DJGLF,Memo_DJGLF,SJ,Memo_SJ,ZJCB,Memo_ZJCB,";
                sql += " GSGLF,Memo_GSGLF,Memo_YWF,LRL,Memo_LRL,'"+ HTJE + "',@tempEstId,LPYT,GPRL,ZPRL FROM o_PreEstUser where PreEstId=@tempEstId;";

                sql += " SET @tempid=(SELECT top 1 PreEstId from o_PreEstUserCS ORDER BY PreEstId DESC);";

                sql += " insert INTO o_PECoordFee (CooContent,CooDepart,money,Memo,IsDelete,PreEstId,PreEstType,Quantity,QuoteMoney) ";
                sql += " SELECT CooContent,CooDepart,money,Memo,IsDelete,@tempid,'用户工程前期测算',Quantity,QuoteMoney";
                sql += " FROM o_PECoordFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PETestFee (TestName,money,IsDelete,Memo,PreEstId,Quantity,QuoteMoney,PreEstType)";
                sql += " SELECT TestName,money,IsDelete,Memo,@tempid,Quantity,QuoteMoney,'用户工程前期测算' ";
                sql += " FROM o_PETestFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PEMaterialFeeUser (EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,PreEstId,";
                sql += " EquipPriceId,TabType,PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity)";
                sql += " SELECT EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,@tempid,EquipPriceId,";
                sql += " TabType,'用户工程前期测算',Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity";
                sql += " FROM o_PEMaterialFeeUser where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PEInstallFee (Name,Specification,Content,Unit,Money,Memo,IsDelete,PreEstId,Quantity,QuoteMoney,PreEstType,TabType,DesignQuantity) ";
                sql += " SELECT Name,Specification,Content,Unit,Money,Memo,IsDelete,@tempid,Quantity,QuoteMoney,'用户工程前期测算',TabType,DesignQuantity";
                sql += " FROM o_PEInstallFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PEEquipFee (EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,PreEstId,EquipPriceId,TabType,";
                sql += " PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity) ";
                sql += " SELECT EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,@tempid,EquipPriceId,TabType,'用户工程前期测算',Quantity,";
                sql += " QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity";
                sql += " FROM o_PEEquipFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";


                sql += " DECLARE @tempCivilId int,@tempCivilAddId int;";
                sql += " DECLARE mycursor CURSOR FOR SELECT PECivilPriceId FROM o_PECivilPrice WHERE PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";
                sql += " OPEN mycursor";
                sql += " FETCH NEXT FROM mycursor INTO @tempCivilId";
                sql += " WHILE (@@fetch_status = 0)";
                sql += " BEGIN";

                sql += " INSERT INTO o_PECivilPrice (Name,TaxRate,Unit,UnitPrice,Memo,IsDelete,LogTime,PreEstId,PreEstType,TabType,OrgCivilPriceId,Quantity) ";
                sql += " SELECT Name,TaxRate,Unit,UnitPrice,Memo,IsDelete,LogTime,@tempid,'用户工程前期测算',TabType,OrgCivilPriceId,Quantity";
                sql += " FROM o_PECivilPrice where PECivilPriceId=@tempCivilId;";

                sql += " SET @tempCivilAddId=(SELECT top 1 PECivilPriceId FROM o_PECivilPrice ORDER BY PECivilPriceId DESC);";

                sql += " INSERT INTO o_PECivilPriceSingle (XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,PECivilPriceId,Memo) ";
                sql += " SELECT XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,@tempCivilAddId,Memo FROM o_PECivilPriceSingle";
                sql += " WHERE PECivilPriceId=@tempCivilId;";
                sql += " FETCH NEXT FROM mycursor INTO @tempCivilId;";

                sql += " END";
                sql += " CLOSE mycursor";
                sql += " DEALLOCATE mycursor";

                sql += " END";
                sql += " ";

                BaseDao.execute(sql);


            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

            result.Add("result", "OK");
            return Json(result);
        }


        /// <summary>
        /// ///////////////////////////////////////////////////////////
        /// </summary>
        /// <returns></returns>
        public ActionResult PETestFeeView()
        {
            return View("PETestFeeView");
        }


        public ActionResult QuoteTestFeeTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,TestFeeId,TestName,money,Memo,money as QuoteMoney,1 as Quantity" +
                        "" +
                        " FROM t_d_TestFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "TestFeeId", "TestName", "money", "Memo", "QuoteMoney", "Quantity" });
                SqlNum = "select count(TestFeeId) as TotalNum FROM t_d_TestFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/TestFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult TestFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string sql = "begin ";
            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PETestFee (TestName,money,Memo,PreEstId,Quantity,QuoteMoney,PreEstType,IsDelete) "+
                    "values('"+list[i]["TestName"]+"','"+ list[i]["money"]+"','"+ list[i]["Memo"]+"','"+ list[i]["PreEstId"]+"','"+
                    list[i]["Quantity"] +"','"+ list[i]["QuoteMoney"]+"','"+ list[i]["PreEstType"]+"',0);";
            }
            sql += " end";
            
            try
            {
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        /// <summary>
        /// 前期测算试验费数据
        /// </summary>
        /// <returns></returns>
        public ActionResult EstTestTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,TestFeeId,TestName,money,Memo,PreEstId,Quantity,QuoteMoney,PreEstType" +
                        " FROM o_PETestFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "TestFeeId", "TestName", "money", "Memo", "PreEstId", "Quantity", "QuoteMoney", "PreEstType" });
                SqlNum = "select count(TestFeeId) as TotalNum FROM o_PETestFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/EstTestTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult PETestFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string TestFeeId = obj["TestFeeId"] == null ? null : obj["TestFeeId"].ToString().Replace(" ", "");
            string TestName = obj["TestName"] == null ? null : obj["TestName"].ToString().Replace(" ", "");
            string QuoteMoney = obj["QuoteMoney"] == null ? null : obj["QuoteMoney"].ToString().Replace(" ", "");
            string Quantity = obj["Quantity"] == null ? null : obj["Quantity"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["TestName"] = TestName,
                    ["QuoteMoney"] = QuoteMoney,
                    ["Quantity"] = Quantity,
                    ["Memo"] = Memo,

                };
                dao.update("o_PETestFee", map, "TestFeeId=" + TestFeeId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult PETestFeeDel()
        {
            string TestFeeId = Request.Form["TestFeeId"] == null ? null : Request.Form["TestFeeId"].ToString().Replace(" ", "");
            string sql = "update o_PETestFee set IsDelete=1 where TestFeeId=" + TestFeeId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }
        ////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 协调费部分//////////////////////////////////////////////////////
        /// </summary>
        /// <returns></returns>
        public ActionResult PECoordFeeView()
        {
            return View("PECoordFeeView");
        }

        public ActionResult QuoteCoordFeeTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,CoordFeeId,CooContent,CooDepart,money,Memo,1 as Quantity,money as QuoteMoney" +
                        "" +
                        " FROM t_d_CoordFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "CoordFeeId", "CooContent", "CooDepart", "money", "Memo", "Quantity", "QuoteMoney" });
                SqlNum = "select count(CoordFeeId) as TotalNum FROM t_d_CoordFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/CoordFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult CoordFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string sql = "begin ";
            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PECoordFee (CooContent,CooDepart,money,Memo,PreEstId,Quantity,QuoteMoney,PreEstType,IsDelete) " +
                    "values('" + list[i]["CooContent"] + "','" + list[i]["CooDepart"] + "','" + list[i]["money"] + "','" + list[i]["Memo"] + "','" +
                    list[i]["PreEstId"] + "','" + list[i]["Quantity"] + "','" + list[i]["QuoteMoney"] + "','"+ list[i]["PreEstType"] +"',0);";
            }
            sql += " end";

            try
            {
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }



        public ActionResult PECoordFeeTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,CoordFeeId,CooContent,CooDepart,money,Memo,Quantity,QuoteMoney" +
                        "" +
                        " FROM o_PECoordFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "CoordFeeId", "CooContent", "CooDepart", "money", "Memo", "Quantity", "QuoteMoney" });
                SqlNum = "select count(CoordFeeId) as TotalNum FROM o_PECoordFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/PECoordFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult PECoordFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string CoordFeeId = obj["CoordFeeId"] == null ? null : obj["CoordFeeId"].ToString().Replace(" ", "");
            string CooDepart = obj["CooDepart"] == null ? null : obj["CooDepart"].ToString().Replace(" ", "");
            string QuoteMoney = obj["QuoteMoney"] == null ? null : obj["QuoteMoney"].ToString().Replace(" ", "");
            string Quantity = obj["Quantity"] == null ? null : obj["Quantity"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["CooDepart"] = CooDepart,
                    ["QuoteMoney"] = QuoteMoney,
                    ["Quantity"] = Quantity,
                    ["Memo"] = Memo,

                };
                dao.update("o_PECoordFee", map, "CoordFeeId=" + CoordFeeId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }


        public ActionResult PECoordFeeDel()
        {
            string CoordFeeId = Request.Form["CoordFeeId"] == null ? null : Request.Form["CoordFeeId"].ToString().Replace(" ", "");
            string sql = "update o_PECoordFee set IsDelete=1 where CoordFeeId=" + CoordFeeId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }
        /////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 安装成本
        /// </summary>
        /// <returns></returns>
        public ActionResult InstallView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            if (ProjectType == "用户工程")
            {
                return View("PEInstallFeeView");
            }
            else if (ProjectType== "用户工程前期测算")
            {
                return View("PEInstallFeeView");
            }
            else
            {
                return View("PEInstallFeeMainlyView");
            }

            
        }

        public ActionResult QuoteInstallFeeTable()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,InstallFeeId,Name,Specification,Content,Unit,Money,Memo,1 as Quantity,Money as QuoteMoney" +
                        "" +
                        " FROM t_d_InstallFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "InstallFeeId", "Name", "Specification", "Content", "Unit", "Money", "Memo", "Quantity", "QuoteMoney" });
                SqlNum = "select count(InstallFeeId) as TotalNum FROM t_d_InstallFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/InstallFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult InstallFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string sql = "begin ";
            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PEInstallFee (Name,Specification,Content,Unit,Money,Memo,QuoteMoney,Quantity,PreEstId,PreEstType,TabType,IsDelete) " +
                    "values('" + list[i]["Name"] + "','" + list[i]["Specification"] + "','" + list[i]["Content"] + "','" + list[i]["Unit"] + "','" +
                    list[i]["Money"] + "','" + list[i]["Memo"] + "','" + list[i]["QuoteMoney"] + "','" + list[i]["Quantity"] + "','" + list[i]["PreEstId"] +
                    "','" + list[i]["PreEstType"]  +"','" + list[i]["TabType"] + "',0);";
            }
            sql += " end";

            try
            {
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult PEInstallFeeTable()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,InstallFeeId,Name,Specification,Content,Unit,Money,Memo,Quantity,QuoteMoney,DesignQuantity" +
                        "" +
                        " FROM o_PEInstallFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "InstallFeeId", "Name", "Specification", "Content", "Unit", "Money", "Memo", "Quantity", "DesignQuantity", "QuoteMoney" });
                SqlNum = "select count(InstallFeeId) as TotalNum FROM o_PEInstallFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/InstallFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult PEInstallFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string InstallFeeId = obj["InstallFeeId"] == null ? null : obj["InstallFeeId"].ToString().Replace(" ", "");
            string QuoteMoney = obj["QuoteMoney"] == null ? null : obj["QuoteMoney"].ToString().Replace(" ", "");
            string Quantity = obj["Quantity"] == null ? null : obj["Quantity"].ToString().Replace(" ", "");
            string DesignQuantity = obj["DesignQuantity"] == null ? null : obj["DesignQuantity"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");
            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string Content = obj["Content"] == null ? null : obj["Content"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["Content"]= Content,
                    ["QuoteMoney"] = QuoteMoney,
                    ["Quantity"] = Quantity,
                    ["Memo"] = Memo,
                    ["DesignQuantity"]= DesignQuantity,

                };
                dao.update("o_PEInstallFee", map, "InstallFeeId=" + InstallFeeId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult PEInstallFeeDel()
        {
            string InstallFeeId = Request.Form["InstallFeeId"] == null ? null : Request.Form["InstallFeeId"].ToString().Replace(" ", "");
            string sql = "update o_PEInstallFee set IsDelete=1 where InstallFeeId=" + InstallFeeId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>
        /// 设备成本
        /// </summary>
        /// <returns></returns>
        public ActionResult PEEquipFeeView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            if (ProjectType == "用户工程")
            {
                return View("PEEquipFeeView");
            }
            else if (ProjectType == "用户工程前期测算")
            {
                return View("PEEquipFeeView");
            }
            else
            {
                return View("PEEquipFeeMainlyView");
            }

            
        }


        public ActionResult QuoteEquipTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,Code,EquipPriceId,EquipName,Specification,Unit,Type,AvgPrice,NormalPrice as QuoteMoney,1 as Quantity" +
                        ",NormalName,NormalPrice,NormalSpeci,TypeL1" +
                        " FROM t_d_EquipPrice " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "Code", "NormalName", "NormalPrice", "NormalSpeci", "EquipPriceId", "EquipName", "Specification", "Unit", "Type", "TypeL1", "AvgPrice", "QuoteMoney", "Quantity" });
                SqlNum = "select count(EquipPriceId) as TotalNum FROM t_d_EquipPrice where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/QuoteEquipTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }







        public ActionResult QuoteEquipPriceSingleTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,id,SellerName,Price,ProjectName,CONVERT(varchar(100),Buytime,23) as Buytime" +
                        "" +
                        " FROM t_d_EquipPriceSingle " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "id", "SellerName", "Price", "ProjectName", "Buytime" });
                SqlNum = "select count(id) as TotalNum FROM t_d_EquipPriceSingle where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/QuoteEquipPriceSingleTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult EquipFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string sql = "begin ";
            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PEEquipFee (EquipName,Specification,Unit,Type,TypeL1,Price,Code,QuoteMoney,Quantity,EquipPriceId,PreEstId,PreEstType,TabType,IsDelete,NormalName,NormalPrice,NormalSpeci) " +
                    "values('" + list[i]["EquipName"] + "','" + list[i]["Specification"] + "','" + list[i]["Unit"] + "','" + list[i]["Type"] + "','" + list[i]["TypeL1"] + "','"+
                    list[i]["AvgPrice"] + "','" + list[i]["Code"] + "','" + list[i]["QuoteMoney"] + "','" + list[i]["Quantity"] + "','" + list[i]["EquipPriceId"] +
                    "','" + list[i]["PreEstId"] + "','" + list[i]["PreEstType"] + "','" + list[i]["TabType"] + "',0,'"+ list[i]["NormalName"] +"',"+ list[i]["NormalPrice"] + ",'"+ list[i]["NormalSpeci"] + "');";
            }
            sql += " end";

            try
            {
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EquipFeeMainlyTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,MaterialId,Type,Code,Specification,Unit,RecoPrice as Price,CONVERT(varchar(100),LogTime,23) as LogTime,Weight," +
                        " 1 as Quantity,RecoPrice as QuoteMoney" +
                        " FROM t_d_Material " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "MaterialId", "Type", "Code", "Specification", "Unit", "Price", "LogTime", "Weight", "Quantity", "QuoteMoney" });
                SqlNum = "select count(MaterialId) as TotalNum FROM t_d_Material where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/EquipFeeMainlyTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }




        public ActionResult EquipFeeMainlyAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string sql = "begin ";
            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PEEquipFee (Specification,Unit,Type,Price,Code,QuoteMoney,Quantity,EquipPriceId,PreEstId,PreEstType,IsDelete) " +
                    "values('" + list[i]["Specification"] + "','" + list[i]["Unit"] + "','" + list[i]["Type"] + "','" +
                    list[i]["Price"] + "','" + list[i]["Code"] + "','" + list[i]["QuoteMoney"] + "','" + list[i]["Quantity"] + "','" + list[i]["MaterialId"] +
                    "','" + list[i]["PreEstId"] + "','" + list[i]["PreEstType"] + "',0);";
            }
            sql += " end";

            try
            {
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }



        public ActionResult PEEquipTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,Code,PEEquipPriceId,EquipName,Specification,Unit,Type,Price,EquipPriceId,QuoteMoney,Quantity,DesignQuantity,SumTax,NormalPrice" +
                        "" +
                        " FROM o_PEEquipFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "Code", "PEEquipPriceId", "EquipName", "Specification", "Unit", "Type", "Price", "EquipPriceId", "QuoteMoney", "Quantity", "DesignQuantity", "SumTax", "NormalPrice" });
                SqlNum = "select count(PEEquipPriceId) as TotalNum FROM o_PEEquipFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/PEEquipTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult PEEquipFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string PEEquipPriceId = obj["PEEquipPriceId"] == null ? null : obj["PEEquipPriceId"].ToString().Replace(" ", "");
            string QuoteMoney = obj["QuoteMoney"] == null ? null : obj["QuoteMoney"].ToString().Replace(" ", "");
            string DesignQuantity = obj["DesignQuantity"] == null ? null : obj["DesignQuantity"].ToString().Replace(" ", "");
            string Quantity = obj["Quantity"] == null ? null : obj["Quantity"].ToString().Replace(" ", "");
            string EquipName = obj["EquipName"] == null ? null : obj["EquipName"].ToString();
            string Specification = obj["Specification"] == null ? null : obj["Specification"].ToString();



            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {

                    ["QuoteMoney"] = QuoteMoney,
                    ["DesignQuantity"]= DesignQuantity,
                    ["Quantity"] = Quantity,
                    ["EquipName"]= EquipName,
                    ["Specification"]= Specification,



                };
                dao.update("o_PEEquipFee", map, "PEEquipPriceId=" + PEEquipPriceId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult PEEquipFeeMainlyUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string PEEquipPriceId = obj["PEEquipPriceId"] == null ? null : obj["PEEquipPriceId"].ToString().Replace(" ", "");
            string SumTax = obj["SumTax"] == null ? null : obj["SumTax"].ToString().Replace(" ", "");
            string Quantity = obj["Quantity"] == null ? null : obj["Quantity"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {

                    ["SumTax"] = SumTax,
                    ["Quantity"] = Quantity,


                };
                dao.update("o_PEEquipFee", map, "PEEquipPriceId=" + PEEquipPriceId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult PEEquipFeeDel()
        {
            string PEEquipPriceId = Request.Form["PEEquipPriceId"] == null ? null : Request.Form["PEEquipPriceId"].ToString().Replace(" ", "");
            string sql = "update o_PEEquipFee set IsDelete=1 where PEEquipPriceId=" + PEEquipPriceId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult PEEquipFeeBatchDel()
        {
            string[] Ids = Request.Form.GetValues("PEEquipPriceId[]");
            string temp = "";
            for (int i = 0; i < Ids.Length; i++)
            {
                temp += Ids[i] + ",";
            }
            temp = temp.Substring(0, temp.Length - 1);
            string sql = "update o_PEEquipFee set IsDelete=1 where PEEquipPriceId in (" + temp + ")";

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EquipFeeMainlyUpload()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string PreEstType = Request.Form["PreEstType"] == null ? null : Request.Form["PreEstType"].ToString().Replace(" ", "");

            string tpye = Request.Files[0].ContentType;
            string UpName = Request.Files[0].FileName;
            //获取后缀名
            string exName = UpName.Substring(UpName.LastIndexOf(".") + 1, (UpName.Length - UpName.LastIndexOf(".") - 1));
            string fileName = "File" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + exName;
            if (Directory.Exists(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd"))) == false)//如果不存在就创建file文件夹
            {
                Directory.CreateDirectory(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd")));
            }
            string filePhysicalPath = Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd") + "/" + fileName);
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {


                Request.Files[0].SaveAs(filePhysicalPath);

                string ErrorMsg = "";
                ErrorMsg = ExcelTool.ImportEquipFeeMainly(filePhysicalPath, PreEstId, PreEstType);
                if (ErrorMsg == "OK")
                {
                    result.Add("result", "OK");
                    return Json(result);
                }
                else
                {
                    result.Add("result", ErrorMsg);
                    return Json(result);
                }


                //upImg.SaveAs(filePhysicalPath);
                //pic = "/upload/" + fileName;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }






        ////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>
        /// 物料成本
        /// </summary>
        /// <returns></returns>
        public ActionResult PEMaterialFeeView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            if (ProjectType == "用户工程")
            {
                return View("PEMaterialFeeView");
            }
            else if(ProjectType== "用户工程前期测算")
            {
                return View("PEMaterialFeeView");
            }
            else
            {
                return View("PEMaterialFeeMainlyView");
            }



            
        }

        public ActionResult MaterialFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string sql = "begin ";
            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PEMaterialFeeUser (EquipName,Specification,Unit,Type,TypeL1,Price,Code,QuoteMoney,Quantity,EquipPriceId,PreEstId,PreEstType,TabType,IsDelete,NormalName,NormalPrice,NormalSpeci) " +
                    "values('" + list[i]["EquipName"] + "','" + list[i]["Specification"] + "','" + list[i]["Unit"] + "','" + list[i]["Type"] + "','" + list[i]["TypeL1"] + "','" +
                    list[i]["AvgPrice"] + "','" + list[i]["Code"] + "','" + list[i]["QuoteMoney"] + "','" + list[i]["Quantity"] + "','" + list[i]["EquipPriceId"] +
                    "','" + list[i]["PreEstId"] + "','" + list[i]["PreEstType"] + "','" + list[i]["TabType"] + "',0,'" + list[i]["NormalName"] + "'," + list[i]["NormalPrice"] + ",'" + list[i]["NormalSpeci"] + "');";
            }
            sql += " end";

            try
            {
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult MaterialFeeMainlyAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string sql = "begin ";
            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PEMaterialFeeUser (Specification,Unit,Type,Price,Code,QuoteMoney,Quantity,EquipPriceId,PreEstId,PreEstType,IsDelete) " +
                    "values('" + list[i]["Specification"] + "','" + list[i]["Unit"] + "','" + list[i]["Type"] + "','" +
                    list[i]["Price"] + "','" + list[i]["Code"] + "','" + list[i]["QuoteMoney"] + "','" + list[i]["Quantity"] + "','" + list[i]["MaterialId"] +
                    "','" + list[i]["PreEstId"] + "','" + list[i]["PreEstType"] + "',0);";
            }
            sql += " end";

            try
            {
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult MaterialFeeMainlyUpload()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string PreEstType = Request.Form["PreEstType"] == null ? null : Request.Form["PreEstType"].ToString().Replace(" ", "");

            string tpye = Request.Files[0].ContentType;
            string UpName = Request.Files[0].FileName;
            //获取后缀名
            string exName = UpName.Substring(UpName.LastIndexOf(".") + 1, (UpName.Length - UpName.LastIndexOf(".") - 1));
            string fileName = "File" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + exName;
            if (Directory.Exists(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd"))) == false)//如果不存在就创建file文件夹
            {
                Directory.CreateDirectory(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd")));
            }
            string filePhysicalPath = Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd") + "/" + fileName);
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {


                Request.Files[0].SaveAs(filePhysicalPath);

                string ErrorMsg = "";
                ErrorMsg = ExcelTool.ImportMaterialFeeMainly(filePhysicalPath, PreEstId, PreEstType);
                if (ErrorMsg == "OK")
                {
                    result.Add("result", "OK");
                    return Json(result);
                }
                else
                {
                    result.Add("result", ErrorMsg);
                    return Json(result);
                }


                //upImg.SaveAs(filePhysicalPath);
                //pic = "/upload/" + fileName;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }






        public ActionResult PEMaterialTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,Code,PEMaterialPriceId,EquipName,Specification,Unit,Type,Price,EquipPriceId,QuoteMoney,Quantity,DesignQuantity,SumTax,NormalPrice" +
                        "" +
                        " FROM o_PEMaterialFeeUser " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "Code", "PEMaterialPriceId", "EquipName", "Specification", "Unit", "Type", "Price", "EquipPriceId", "QuoteMoney", "Quantity", "DesignQuantity", "SumTax", "NormalPrice" });
                SqlNum = "select count(PEMaterialPriceId) as TotalNum FROM o_PEMaterialFeeUser where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/PEMaterialTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult PEMaterialFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string PEMaterialPriceId = obj["PEMaterialPriceId"] == null ? null : obj["PEMaterialPriceId"].ToString().Replace(" ", "");
            string QuoteMoney = obj["QuoteMoney"] == null ? null : obj["QuoteMoney"].ToString().Replace(" ", "");
            string DesignQuantity = obj["DesignQuantity"] == null ? null : obj["DesignQuantity"].ToString().Replace(" ", "");
            string Quantity = obj["Quantity"] == null ? null : obj["Quantity"].ToString().Replace(" ", "");
            string EquipName = obj["EquipName"] == null ? null : obj["EquipName"].ToString().Replace(" ", "");
            string Specification = obj["Specification"] == null ? null : obj["Specification"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {

                    ["QuoteMoney"] = QuoteMoney,
                    ["Quantity"] = Quantity,
                    ["EquipName"]= EquipName,
                    ["Specification"]= Specification,
                    ["DesignQuantity"]= DesignQuantity,


                };
                dao.update("o_PEMaterialFeeUser", map, "PEMaterialPriceId=" + PEMaterialPriceId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult PEMaterialFeeMainlyUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string PEMaterialPriceId = obj["PEMaterialPriceId"] == null ? null : obj["PEMaterialPriceId"].ToString().Replace(" ", "");
            string SumTax = obj["SumTax"] == null ? null : obj["SumTax"].ToString().Replace(" ", "");
            string Quantity = obj["Quantity"] == null ? null : obj["Quantity"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {

                    ["SumTax"] = SumTax,
                    ["Quantity"] = Quantity,


                };
                dao.update("o_PEMaterialFeeUser", map, "PEMaterialPriceId=" + PEMaterialPriceId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult PEMaterialFeeDel()
        {
            string PEMaterialPriceId = Request.Form["PEMaterialPriceId"] == null ? null : Request.Form["PEMaterialPriceId"].ToString().Replace(" ", "");
            string sql = "update o_PEMaterialFeeUser set IsDelete=1 where PEMaterialPriceId=" + PEMaterialPriceId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult PEMaterialFeeBatchDel()
        {
            string[] Ids = Request.Form.GetValues("PEMaterialPriceId[]");
            string temp = "";
            for (int i = 0; i < Ids.Length; i++)
            {
                temp += Ids[i] + ",";
            }
            temp = temp.Substring(0, temp.Length - 1);
            string sql = "update o_PEMaterialFeeUser set IsDelete=1 where PEMaterialPriceId in (" + temp + ")";

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        /////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>


        public ActionResult PECivilPriceView()
        {
            return View("PECivilPriceView");
        }

        public ActionResult PECivilPriceAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string TimeStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DateTime TempTime = DateTime.Parse(TimeStr);




            string sql = "begin ";



            for (int i = 0; i < list.Count; i++)
            {
                sql += "insert into o_PECivilPrice (Name,TaxRate,Unit,UnitPrice,Memo,OrgCivilPriceId,LogTime,PreEstId,PreEstType,TabType,IsDelete) " +
                    "values('" + list[i]["Name"] + "','" + list[i]["TaxRate"] + "','" + list[i]["Unit"] + "','" + list[i]["UnitPrice"] + "','" +
                    list[i]["Memo"] + "','" + list[i]["CivilPriceId"] + "','" + TempTime.AddMilliseconds(i*10).ToString("yyyy-MM-dd HH:mm:ss.fff") + "','" + list[i]["PreEstId"] + 
                    "','" + list[i]["PreEstType"] + "','" + list[i]["TabType"] +  "',0);";
            }

            sql += " end";

            string sql2 = "begin DECLARE @id int;";

            for (int i = 0; i < list.Count; i++)
            {
                sql2 += " SET @id=(SELECT PECivilPriceId FROM o_PECivilPrice WHERE LogTime='" + TempTime.AddMilliseconds(i * 10).ToString("yyyy-MM-dd HH:mm:ss.fff") + "');";
                sql2 += " INSERT INTO o_PECivilPriceSingle(XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,PECivilPriceId,Memo) " +
                    "SELECT XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,@id,Memo FROM t_d_CivilPriceSingle WHERE CivilPriceId=" + list[i]["CivilPriceId"] + ";";

            }
            sql2 += " end";

            try
            {
                BaseDao.execute(sql);
                BaseDao.execute(sql2);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult PECivilTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,PECivilPriceId,Name,TaxRate,Unit,UnitPrice,Memo,Quantity" +
                        "" +
                        " FROM o_PECivilPrice " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "PECivilPriceId", "Name", "TaxRate", "Unit", "UnitPrice", "Memo", "Quantity" });
                SqlNum = "select count(PECivilPriceId) as TotalNum FROM o_PECivilPrice where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/PECivilTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }



        public ActionResult CivilPriceSingleEdit()
        {
            string PECivilPriceId = Request.QueryString["PECivilPriceId"].ToString();
            string sql = "select * from o_PECivilPrice where PECivilPriceId=" + PECivilPriceId;
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            list = dao.GetList(sql, new string[] { "PECivilPriceId", "Name", "TaxRate", "Unit", "UnitPrice", "Memo" });

            ViewBag.PECivilPriceId = PECivilPriceId;
            ViewBag.Name = list[0]["Name"].ToString();
            ViewBag.TaxRate = list[0]["TaxRate"].ToString();
            ViewBag.Unit = list[0]["Unit"].ToString();
            ViewBag.UnitPrice = list[0]["UnitPrice"].ToString();
            ViewBag.Memo = list[0]["Memo"].ToString();


            return View("PECivilPriceSingleEditView");
        }


        public ActionResult CivilSingleTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,PECivilPriceId,XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,Memo,Id" +
                        "" +
                        " FROM o_PECivilPriceSingle " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "PECivilPriceId", "XMMC", "GCLJSS", "DW", "GCL", "RGDJ", "CLDJ", "Memo", "Id" });
                SqlNum = "select count(PECivilPriceId) as TotalNum FROM o_PECivilPriceSingle where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/CivilSingleTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult CivilPriceUpdate_2()
        {
            string PECivilPriceId = Request.Form["PECivilPriceId"] == null ? null : Request.Form["PECivilPriceId"].ToString().Replace(" ", "");
            string Quantity = Request.Form["Quantity"] == null ? null : Request.Form["Quantity"].ToString().Replace(" ", "");
            string Memo = Request.Form["Memo"] == null ? null : Request.Form["Memo"].ToString().Replace(" ", "");

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Quantity"] = Quantity,
                    ["Memo"] = Memo,
                };
                dao.update("o_PECivilPrice", map, "PECivilPriceId=" + PECivilPriceId);
                result.Add("result", "OK");
                return Json(result);

            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }



        public ActionResult CivilPriceUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string PECivilPriceId = obj["PECivilPriceId"] == null ? null : obj["PECivilPriceId"].ToString().Replace(" ", "");
            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string TaxRate = obj["TaxRate"] == null ? null : obj["TaxRate"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            if (TaxRate == "") TaxRate = "0.033";
            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["TaxRate"] = TaxRate,
                    ["Unit"] = Unit,
                    ["Memo"] = Memo,

                };
                dao.update("o_PECivilPrice", map, "PECivilPriceId=" + PECivilPriceId);

                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                string sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from o_PECivilPriceSingle where PECivilPriceId=" + PECivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql = "select TaxRate from o_PECivilPrice where PECivilPriceId=" + PECivilPriceId;
                string Tax = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(Tax);
                toltalnum = sumnum + sumnum * taxnum;
                sql = "update o_PECivilPrice set UnitPrice=" + toltalnum.ToString("f2") + " where PECivilPriceId=" + PECivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult CivilPriceSingleUpdate()
        {
             Dictionary<string, object> result = new Dictionary<string, object>();

            string PECivilPriceId = Request.Form["PECivilPriceId"] == null ? null : Request.Form["PECivilPriceId"].ToString().Replace(" ", "");
            string Id = Request.Form["Id"] == null ? null : Request.Form["Id"].ToString().Replace(" ", "");
            string XMMC = Request.Form["XMMC"] == null ? null : Request.Form["XMMC"].ToString().Replace(" ", "");
            string GCLJSS = Request.Form["GCLJSS"] == null ? null : Request.Form["GCLJSS"].ToString().Replace(" ", "");
            string DW = Request.Form["DW"] == null ? null : Request.Form["DW"].ToString().Replace(" ", "");
            string GCL = Request.Form["GCL"] == null ? null : Request.Form["GCL"].ToString().Replace(" ", "");
            string RGDJ = Request.Form["RGDJ"] == null ? null : Request.Form["RGDJ"].ToString().Replace(" ", "");
            string CLDJ = Request.Form["CLDJ"] == null ? null : Request.Form["CLDJ"].ToString().Replace(" ", "");
            string Memo = Request.Form["Memo"] == null ? null : Request.Form["Memo"].ToString().Replace(" ", "");


            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["XMMC"] = XMMC,
                    ["GCLJSS"] = GCLJSS,
                    ["DW"] = DW,
                    ["GCL"] = GCL,
                    ["RGDJ"] = RGDJ,
                    ["CLDJ"] = CLDJ,
                    ["Memo"] = Memo,
                };
                dao.update("o_PECivilPriceSingle", map, "Id=" + Id);
                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                string sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from o_PECivilPriceSingle where PECivilPriceId=" + PECivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql = "select TaxRate from o_PECivilPrice where PECivilPriceId=" + PECivilPriceId;
                string TaxRate = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(TaxRate);
                toltalnum = sumnum + sumnum * taxnum;
                sql = "update o_PECivilPrice set UnitPrice=" + toltalnum.ToString("f2") + " where PECivilPriceId=" + PECivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult CivilPriceSingleAdd()
        {

            Dictionary<string, object> result = new Dictionary<string, object>();

            string PECivilPriceId = Request.Form["PECivilPriceId"] == null ? null : Request.Form["PECivilPriceId"].ToString().Replace(" ", "");
            string XMMC = Request.Form["XMMC"] == null ? null : Request.Form["XMMC"].ToString().Replace(" ", "");
            string GCLJSS = Request.Form["GCLJSS"] == null ? null : Request.Form["GCLJSS"].ToString().Replace(" ", "");
            string DW = Request.Form["DW"] == null ? null : Request.Form["DW"].ToString().Replace(" ", "");
            string GCL = Request.Form["GCL"] == null ? null : Request.Form["GCL"].ToString().Replace(" ", "");
            string RGDJ = Request.Form["RGDJ"] == null ? null : Request.Form["RGDJ"].ToString().Replace(" ", "");
            string CLDJ = Request.Form["CLDJ"] == null ? null : Request.Form["CLDJ"].ToString().Replace(" ", "");
            string Memo = Request.Form["Memo"] == null ? null : Request.Form["Memo"].ToString().Replace(" ", "");


            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["PECivilPriceId"] = PECivilPriceId,
                    ["XMMC"] = XMMC,
                    ["GCLJSS"] = GCLJSS,
                    ["DW"] = DW,
                    ["GCL"] = GCL,
                    ["RGDJ"] = RGDJ,
                    ["CLDJ"] = CLDJ,
                    ["Memo"] = Memo,
                };
                dao.save("o_PECivilPriceSingle", map);
                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                string sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from o_PECivilPriceSingle where PECivilPriceId=" + PECivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql = "select TaxRate from o_PECivilPrice where PECivilPriceId=" + PECivilPriceId;
                string TaxRate = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(TaxRate);
                toltalnum = sumnum + sumnum * taxnum;
                sql = "update o_PECivilPrice set UnitPrice=" + toltalnum.ToString("f2") + " where PECivilPriceId=" + PECivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult CivilPriceSingleDel()
        {
            string PECivilPriceId = Request.Form["PECivilPriceId"] == null ? null : Request.Form["PECivilPriceId"].ToString().Replace(" ", "");
            string Id = Request.Form["Id"] == null ? null : Request.Form["Id"].ToString().Replace(" ", "");
            string sql = "delete o_PECivilPriceSingle where Id=" + Id;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from o_PECivilPriceSingle where PECivilPriceId=" + PECivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql = "select TaxRate from o_PECivilPrice where PECivilPriceId=" + PECivilPriceId;
                string TaxRate = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(TaxRate);
                toltalnum = sumnum + sumnum * taxnum;
                sql = "update o_PECivilPrice set UnitPrice=" + toltalnum.ToString("f2") + " where PECivilPriceId=" + PECivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult CivilBriefView()
        {
            string PECivilPriceId = Request.QueryString["PECivilPriceId"].ToString();
            string sql = "select * from o_PECivilPrice where PECivilPriceId=" + PECivilPriceId;
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            list = dao.GetList(sql, new string[] { "PECivilPriceId", "Name", "TaxRate", "Unit", "UnitPrice", "Memo" });

            ViewBag.PECivilPriceId = PECivilPriceId;
            ViewBag.Name = list[0]["Name"].ToString();
            ViewBag.TaxRate = list[0]["TaxRate"].ToString();
            ViewBag.Unit = list[0]["Unit"].ToString();
            ViewBag.UnitPrice = list[0]["UnitPrice"].ToString();
            ViewBag.Memo = list[0]["Memo"].ToString();

            ViewBag.PECivilPriceId = PECivilPriceId;
            return View("PECivilBriefView");

        }

        public ActionResult CivilPriceDel()
        {
            string PECivilPriceId = Request.Form["PECivilPriceId"] == null ? null : Request.Form["PECivilPriceId"].ToString().Replace(" ", "");
            string sql = "update o_PECivilPrice set IsDelete=1 where PECivilPriceId=" + PECivilPriceId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        ////////////////////////////////////////////////////
        /// <summary>
        /// 前期测算  用户工程  汇总表
        /// </summary>
        /// <returns></returns>
        public ActionResult PEView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            if (ProjectType == "用户工程")
            {

                var map1 = GetPEUserSummary(PreEstId, ProjectType);
                string sql = "select * from o_PreEstUser where PreEstId=" + PreEstId;
                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","LPYT","GPRL","ZPRL" };
                map2 = dao.GetList(sql, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.DataMap = map1;

                return View("PEUserView");
            }
            else if(ProjectType == "用户工程前期测算")
            {
                //
                var map1 = GetPEUserSummary(PreEstId, ProjectType);
                string sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId","LPYT","GPRL","ZPRL" };
                map2 = dao.GetList(sql, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.MapCS = map1;

                var map3 = GetPEUserSummary(map1["PreEstUserId"].ToString(), "用户工程");
                sql = "select * from o_PreEstUser where PreEstId=" + map1["PreEstUserId"].ToString();
                Dictionary<string, object> map4 = new Dictionary<string, object>();
                string[] field2 = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","LPYT","GPRL","ZPRL" };
                map4 = dao.GetList(sql, field2)[0];
                map3 = map3.Concat(map4).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.MapBJ = map3;



                return View("PEUserCSView");
            }
            else if(ProjectType == "主业")
            {
                //主业工程前期测算 第一次
                var map1 = GetPEMainlySummary(PreEstId, ProjectType);
                string sql = "select * from o_PreEstMainly where PreEstId=" + PreEstId;

                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "TZZE", "SJF", "Memo_SJF","SJF_F","JLF","Memo_JLF","JLF_F", "Memo_JGCL", "Memo_JGSB", "JHTJSGF","JHTJSGF_F","Memo_JHTJSGF", "JHAZSGF_F","JHAZSGF", "Memo_JHAZSGF","JHZC_F","JHZC","Memo_JHZC"
                    ,"JHSYF_F","JHSYF","Memo_JHSYF","JHBJSGF","JHBJSGF_F","Memo_JHBJSGF","JHPCF_F","JHPCF","Memo_JHPCF",
                "Memo_CBTJSGF", "Memo_CBAZSGF", "CBZC", "Memo_CBZC", "Memo_CBSYF", "CBBJSGF", "Memo_CBBJSGF", "CBPCF","Memo_CBPCF","Memo_CBXTF","CBSJ","Memo_CBSJ","CBGSGLF","Memo_CBGSGLF","Memo_HJ","Memo_MLR" };
                map2 = dao.GetList(sql, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.DataMap = map1;


                return View("PEMainlyView");
            }
            else
            {
                //主业工程前期测算结算
                var map1 = GetPEMainlySummary(PreEstId, ProjectType);
                string sql = "select * from o_PreEstMainlyJS where PreEstId=" + PreEstId;

                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "TZZE", "SJF", "Memo_SJF","SJF_F","JLF","Memo_JLF","JLF_F", "Memo_JGCL", "Memo_JGSB", "JHTJSGF","JHTJSGF_F","Memo_JHTJSGF", "JHAZSGF_F","JHAZSGF", "Memo_JHAZSGF","JHZC_F","JHZC","Memo_JHZC"
                    ,"JHSYF_F","JHSYF","Memo_JHSYF","JHBJSGF","JHBJSGF_F","Memo_JHBJSGF","JHPCF_F","JHPCF","Memo_JHPCF",
                "Memo_CBTJSGF", "Memo_CBAZSGF", "CBZC", "Memo_CBZC", "Memo_CBSYF", "CBBJSGF", "Memo_CBBJSGF", "CBPCF","Memo_CBPCF","Memo_CBXTF","CBSJ","Memo_CBSJ","CBGSGLF","Memo_CBGSGLF","Memo_HJ","Memo_MLR" };
                map2 = dao.GetList(sql, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.DataMap = map1;


                return View("PEMainlyViewJS");



            }
            
        }

        //用户工程前期测算对比
        public ActionResult CSContrastView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            //获取报价测算，前期测算的数据
            var map1 = GetPEUserSummary(PreEstId, ProjectType);
            string sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
            Dictionary<string, object> map2 = new Dictionary<string, object>();
            string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId" };
            map2 = dao.GetList(sql, field)[0];
            map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.MapCS = map1;

            var map3 = GetPEUserSummary(map1["PreEstUserId"].ToString(), "用户工程");
            sql = "select * from o_PreEstUser where PreEstId=" + map1["PreEstUserId"].ToString();
            Dictionary<string, object> map4 = new Dictionary<string, object>();
            string[] field2 = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL" };
            map4 = dao.GetList(sql, field2)[0];
            map3 = map3.Concat(map4).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.MapBJ = map3;

            //获取前期测算参考与实际价格
            var map5 = GetPECKMoney(PreEstId, ProjectType);     //参考价格
            var map6 = GetPESJMoney(PreEstId, ProjectType);     //实际价格
            ViewBag.MapCSCK = map5;
            ViewBag.MapCSSJ = map6;

            //获取报价测算参考与实际价格
            var map7= GetPECKMoney(map1["PreEstUserId"].ToString(), "用户工程");    //参考价格
            var map8 = GetPESJMoney(map1["PreEstUserId"].ToString(), "用户工程");   //实际价格
            ViewBag.MapBJCK = map7;
            ViewBag.MapBJSJ = map8;

            return View("YHCSContrastView");
        }

        //用户工程报价测算对比
        public ActionResult YHBJContrastView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            var map3 = GetPEUserSummary(PreEstId, ProjectType);
            string sql = "select * from o_PreEstUser where PreEstId=" + PreEstId;
            Dictionary<string, object> map4 = new Dictionary<string, object>();
            string[] field2 = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL" };
            map4 = dao.GetList(sql, field2)[0];
            map3 = map3.Concat(map4).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.MapBJ = map3;

            //获取报价测算参考与实际价格
            var map7 = GetPECKMoney(PreEstId, ProjectType);    //参考价格
            var map8 = GetPESJMoney(PreEstId, ProjectType);   //实际价格
            ViewBag.MapBJCK = map7;
            ViewBag.MapBJSJ = map8;


            return View("YHBJContrastView");
        }


        //主业工程报价测算对比
        public ActionResult ZYCSContrastView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            var map1 = GetPEMainlySummary(PreEstId, ProjectType);
            string sql = "select * from o_PreEstMainly where PreEstId=" + PreEstId;

            Dictionary<string, object> map2 = new Dictionary<string, object>();
            string[] field = new string[] { "TZZE", "SJF", "Memo_SJF","SJF_F","JLF","Memo_JLF","JLF_F", "Memo_JGCL", "Memo_JGSB", "JHTJSGF","JHTJSGF_F","Memo_JHTJSGF", "JHAZSGF_F","JHAZSGF", "Memo_JHAZSGF","JHZC_F","JHZC","Memo_JHZC"
                    ,"JHSYF_F","JHSYF","Memo_JHSYF","JHBJSGF","JHBJSGF_F","Memo_JHBJSGF","JHPCF_F","JHPCF","Memo_JHPCF",
                "Memo_CBTJSGF", "Memo_CBAZSGF", "CBZC", "Memo_CBZC", "Memo_CBSYF", "CBBJSGF", "Memo_CBBJSGF", "CBPCF","Memo_CBPCF","Memo_CBXTF","CBSJ","Memo_CBSJ","CBGSGLF","Memo_CBGSGLF","Memo_HJ","Memo_MLR" };
            map2 = dao.GetList(sql, field)[0];
            map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.MapBJ = map1;

            //获取报价测算参考与实际价格
            var map7 = GetPECKMoney(PreEstId, ProjectType);    //参考价格
            var map8 = GetPESJMoney(PreEstId, ProjectType);   //实际价格
            ViewBag.MapBJCK = map7;
            ViewBag.MapBJSJ = map8;



            return View("ZYCSContrastView");
        }
        

        //主业结算对比表
        public ActionResult ZYJSContrastView()
        {
            string PreEstId = Request.QueryString["PreEstId"] == null ? null : Request.QueryString["PreEstId"].ToString().Replace(" ", "");
            string ProjectType = Request.QueryString["ProjectType"] == null ? null : Request.QueryString["ProjectType"].ToString().Replace(" ", "");

            var map1 = GetPEMainlySummary(PreEstId, ProjectType);
            string sql = "select * from o_PreEstMainlyJS where PreEstId=" + PreEstId;

            Dictionary<string, object> map2 = new Dictionary<string, object>();
            string[] field = new string[] { "TZZE", "SJF", "Memo_SJF","SJF_F","JLF","Memo_JLF","JLF_F", "Memo_JGCL", "Memo_JGSB", "JHTJSGF","JHTJSGF_F","Memo_JHTJSGF", "JHAZSGF_F","JHAZSGF", "Memo_JHAZSGF","JHZC_F","JHZC","Memo_JHZC"
                    ,"JHSYF_F","JHSYF","Memo_JHSYF","JHBJSGF","JHBJSGF_F","Memo_JHBJSGF","JHPCF_F","JHPCF","Memo_JHPCF",
                "Memo_CBTJSGF", "Memo_CBAZSGF", "CBZC", "Memo_CBZC", "Memo_CBSYF", "CBBJSGF", "Memo_CBBJSGF", "CBPCF","Memo_CBPCF","Memo_CBXTF","CBSJ","Memo_CBSJ","CBGSGLF","Memo_CBGSGLF","Memo_HJ","Memo_MLR" };
            map2 = dao.GetList(sql, field)[0];
            map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.MapJS = map1;

            //获取报价测算参考与实际价格
            var map7 = GetPECKMoney(PreEstId, ProjectType);    //参考价格
            var map8 = GetPESJMoney(PreEstId, ProjectType);   //实际价格
            ViewBag.MapJSCK = map7;
            ViewBag.MapJSSJ = map8;

            return View("ZYJSContrastView");
        }


        /// <summary>
        /// 查汇总表信息
        /// </summary>
        /// <param name="PreEstId"></param>
        /// <param name="PreEstType"></param>
        /// <returns></returns>
        public Dictionary<string ,object> GetPEUserSummary(string PreEstId,string PreEstType)
        {
            string sql = "begin ";
            sql += " DECLARE @CoordFee FLOAT,@TestFee FLOAT,@MeterialFee_WX FLOAT,@MeterialFee_KBS FLOAT,@MeterialFee_GP FLOAT,@MeterialFee_ZP FLOAT,@MeterialFee_YX FLOAT;";
            sql += " DECLARE @EquipFee_WX FLOAT,@EquipFee_KBS FLOAT,@EquipFee_GP FLOAT,@EquipFee_ZP FLOAT;";
            sql += " DECLARE @CivilPrice_WX FLOAT,@CivilPrice_HXN FLOAT;";
            sql += " DECLARE @InstallFee_WX FLOAT,@InstallFee_KBS FLOAT,@InstallFee_GP FLOAT,@InstallFee_ZP FLOAT;";

            sql += " SET @CoordFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PECoordFee where IsDelete<>1 and PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @TestFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PETestFee where IsDelete<>1 and  PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @MeterialFee_WX=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEMaterialFeeUser where IsDelete<>1 and TabType='WX' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @MeterialFee_KBS=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEMaterialFeeUser where IsDelete<>1 and TabType='KBS' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @MeterialFee_GP=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEMaterialFeeUser where IsDelete<>1 and TabType='GP' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @MeterialFee_ZP=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEMaterialFeeUser where IsDelete<>1 and TabType='ZP' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @MeterialFee_YX=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEMaterialFeeUser where IsDelete<>1 and TabType='YXZB' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @EquipFee_WX=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEEquipFee where IsDelete<>1 and TabType='WX' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @EquipFee_KBS=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEEquipFee where IsDelete<>1 and TabType='KBS' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @EquipFee_GP=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEEquipFee where IsDelete<>1 and TabType='GP' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @EquipFee_ZP=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEEquipFee where IsDelete<>1 and TabType='ZP' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @CivilPrice_WX=(SELECT SUM(UnitPrice*Quantity) AS num FROM o_PECivilPrice where IsDelete<>1 and TabType='WX' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @CivilPrice_HXN=(SELECT SUM(UnitPrice*Quantity) AS num FROM o_PECivilPrice where IsDelete<>1 and TabType='HXN' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @InstallFee_WX=(SELECT SUM(QuoteMoney*Quantity) AS num FROM o_PEInstallFee WHERE IsDelete<>1 and TabType='WX' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @InstallFee_KBS=(SELECT SUM(QuoteMoney*Quantity) AS num FROM o_PEInstallFee WHERE IsDelete<>1 and TabType='KBS' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @InstallFee_GP=(SELECT SUM(QuoteMoney*Quantity) AS num FROM o_PEInstallFee WHERE IsDelete<>1 and TabType='GP' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @InstallFee_ZP=(SELECT SUM(QuoteMoney*Quantity) AS num FROM o_PEInstallFee WHERE IsDelete<>1 and TabType='ZP' AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " select @CoordFee as CoordFee,@TestFee as TestFee,@MeterialFee_WX as MeterialFee_WX,@MeterialFee_KBS as MeterialFee_KBS,@MeterialFee_GP as MeterialFee_GP,@MeterialFee_ZP as MeterialFee_ZP,@MeterialFee_YX as MeterialFee_YX";
            sql += ",@EquipFee_WX as EquipFee_WX,@EquipFee_KBS as EquipFee_KBS,@EquipFee_GP as EquipFee_GP,@EquipFee_ZP as EquipFee_ZP,@CivilPrice_WX as CivilPrice_WX,@CivilPrice_HXN as CivilPrice_HXN,@InstallFee_WX as InstallFee_WX,@InstallFee_KBS as InstallFee_KBS,@InstallFee_GP as InstallFee_GP";
            sql += ",@InstallFee_ZP as InstallFee_ZP";

            sql += " end";


            Dictionary<string, object> map = new Dictionary<string, object>();
            string[] field = new string[] { "CoordFee", "TestFee", "MeterialFee_WX", "MeterialFee_KBS", "MeterialFee_GP", "MeterialFee_ZP", "MeterialFee_YX", "EquipFee_WX",
                "EquipFee_KBS", "EquipFee_GP", "EquipFee_ZP", "CivilPrice_WX", "CivilPrice_HXN", "InstallFee_WX", "InstallFee_KBS", "InstallFee_GP", "InstallFee_ZP" };

            map = dao.GetList(sql, field)[0];

            return map;
        }

        /// <summary>
        /// 获取前期测算表内参考的价格汇总
        /// </summary>
        /// <param name="PreEstId"></param>
        /// <param name="PreEstType"></param>
        /// <returns></returns>
        public Dictionary<string, object> GetPECKMoney(string PreEstId, string PreEstType)
        {

            string sql = "begin ";
            sql += "DECLARE @CoordFee FLOAT,@TestFee FLOAT,@MeterialFee FLOAT,@EquipFee FLOAT,@CivilPrice FLOAT,@InstallFee FLOAT;";

            sql += "SET @CoordFee=(SELECT SUM(money*Quantity) as num FROM o_PECoordFee where IsDelete<>1 and PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += "SET @TestFee=(SELECT SUM(money*Quantity) as num FROM o_PETestFee where IsDelete<>1 and  PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += "SET @MeterialFee=(SELECT SUM(NormalPrice*Quantity) as num FROM o_PEMaterialFeeUser where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += "SET @EquipFee=(SELECT SUM(NormalPrice*Quantity) as num FROM o_PEEquipFee where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += "SET @CivilPrice=(SELECT SUM(b.UnitPrice*a.Quantity) AS num FROM o_PECivilPrice a INNER JOIN t_d_CivilPrice b ON a.OrgCivilPriceId=b.CivilPriceId";
            sql += " where a.IsDelete<>1 and a.PreEstId=" + PreEstId + " AND a.PreEstType='" + PreEstType + "');";
            sql += "SET @InstallFee=(SELECT SUM(Money*Quantity) AS num FROM o_PEInstallFee WHERE IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += "SELECT @CoordFee as CKCoordFee,@TestFee AS CKTestFee,@MeterialFee AS CKMeterialFee,@EquipFee AS CKEquipFee,@CivilPrice AS CKCivilPrice,@InstallFee as CKInstallFee;";

            sql += "END";

            Dictionary<string, object> map = new Dictionary<string, object>();
            string[] field = new string[] { "CKCoordFee", "CKTestFee", "CKMeterialFee", "CKEquipFee", "CKCivilPrice", "CKInstallFee" };

            map = dao.GetList(sql, field)[0];

            //这里把空值填0
            //foreach (KeyValuePair<string,object>pair in map)
            //{
            //    if( map[pair.Key] == null || map[pair.Key].ToString() == "" )
            //    {
            //        map[pair.Key] = 0;
            //    }
            //}
            for (int i = 0; i < map.Count; i++)
            {
                if(map.ElementAt(i).Value==null || map.ElementAt(i).Value.ToString() == "")
                {
                    map[map.ElementAt(i).Key] = 0;
                }
            }


            return map;

        }


        /// <summary>
        /// 获得前期测算表内实际的价格汇总
        /// </summary>
        /// <param name="PreEstId"></param>
        /// <param name="PreEstType"></param>
        /// <returns></returns>
        public Dictionary<string, object> GetPESJMoney(string PreEstId, string PreEstType)
        {
            string sql = "begin ";
            sql += "DECLARE @CoordFee FLOAT,@TestFee FLOAT,@MeterialFee FLOAT,@EquipFee FLOAT,@CivilPrice FLOAT,@InstallFee FLOAT;";

            sql += " SET @CoordFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PECoordFee where IsDelete<>1 and PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @TestFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PETestFee where IsDelete<>1 and  PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @MeterialFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEMaterialFeeUser where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @EquipFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PEEquipFee where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @CivilPrice=(SELECT SUM(UnitPrice*Quantity) AS num FROM o_PECivilPrice where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";
            sql += " SET @InstallFee=(SELECT SUM(QuoteMoney*Quantity) AS num FROM o_PEInstallFee WHERE IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += "SELECT @CoordFee as CoordFee,@TestFee AS TestFee,@MeterialFee AS MeterialFee,@EquipFee AS EquipFee,@CivilPrice AS CivilPrice,@InstallFee as InstallFee;";

            sql += " END";

            Dictionary<string, object> map = new Dictionary<string, object>();
            string[] field = new string[] { "CoordFee", "TestFee", "MeterialFee", "EquipFee", "CivilPrice", "InstallFee" };

            map = dao.GetList(sql, field)[0];

            //这里把空值填0
            //foreach (KeyValuePair<string, object> pair in map)
            //{
            //    if (map[pair.Key] == null || map[pair.Key].ToString() == "")
            //    {
            //        map[pair.Key] = 0;
            //    }
            //}
            for (int i = 0; i < map.Count; i++)
            {
                if (map.ElementAt(i).Value == null || map.ElementAt(i).Value.ToString() == "")
                {
                    map[map.ElementAt(i).Key] = 0;
                }
            }

            return map;
        }









        public ActionResult PEUserUpdate()
        {
            string FieldName = Request.Form["FieldName"] == null ? null : Request.Form["FieldName"].ToString().Replace(" ", "");
            string FieldValue = Request.Form["FieldValue"] == null ? null : Request.Form["FieldValue"].ToString().Replace(" ", "");
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string PreEstType = Request.Form["PreEstType"] == null ? null : Request.Form["PreEstType"].ToString().Replace(" ", "");
            string sql = "update o_PreEstUser set " + FieldName + "='"+ FieldValue + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                var map1 = GetPEUserSummary(PreEstId, PreEstType);
                string sql2 = "select * from o_PreEstUser where PreEstId=" + PreEstId;
                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","LPYT","GPRL","ZPRL" };
                map2 = dao.GetList(sql2, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                //ViewBag.DataMap = map1;

                result = result.Concat(map1).ToDictionary(k => k.Key, v => v.Value);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult PEUserCSUpdate()
        {
            string FieldName = Request.Form["FieldName"] == null ? null : Request.Form["FieldName"].ToString().Replace(" ", "");
            string FieldValue = Request.Form["FieldValue"] == null ? null : Request.Form["FieldValue"].ToString().Replace(" ", "");
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string PreEstType = Request.Form["PreEstType"] == null ? null : Request.Form["PreEstType"].ToString().Replace(" ", "");
            string sql = "update o_PreEstUserCS set " + FieldName + "='" + FieldValue + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                var map1 = GetPEUserSummary(PreEstId, PreEstType);
                sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId","LPYT","GPRL","ZPRL" };
                map2 = dao.GetList(sql, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                //ViewBag.MapCS = map1;

                var map3 = GetPEUserSummary(map1["PreEstUserId"].ToString(), "用户工程");
                sql = "select * from o_PreEstUser where PreEstId=" + map1["PreEstUserId"].ToString();
                Dictionary<string, object> map4 = new Dictionary<string, object>();
                string[] field2 = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                    "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","LPYT","GPRL","ZPRL" };
                map4 = dao.GetList(sql, field2)[0];
                map3 = map3.Concat(map4).ToDictionary(k => k.Key, v => v.Value);
                //ViewBag.MapBJ = map3;
                //result = result.Concat(map1).ToDictionary(k => k.Key, v => v.Value);

                result.Add("MapCS", map1);
                result.Add("MapBJ", map3);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public Dictionary<string, object> GetPEMainlySummary(string PreEstId, string PreEstType)
        {
            string sql = "begin ";
            sql += " DECLARE @CoordFee FLOAT,@TestFee FLOAT,@MeterialFee FLOAT,@EquipFee FLOAT,@CivilPrice FLOAT,@InstallFee FLOAT;";

            sql += " SET @CoordFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PECoordFee where IsDelete<>1 and PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @TestFee=(SELECT SUM(QuoteMoney*Quantity) as num FROM o_PETestFee where IsDelete<>1 and  PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @MeterialFee=(SELECT SUM(SumTax) as num FROM o_PEMaterialFeeUser where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @EquipFee=(SELECT SUM(SumTax) as num FROM o_PEEquipFee where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @CivilPrice=(SELECT SUM(UnitPrice*Quantity) AS num FROM o_PECivilPrice where IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " SET @InstallFee=(SELECT SUM(QuoteMoney*Quantity) AS num FROM o_PEInstallFee WHERE IsDelete<>1 AND PreEstId=" + PreEstId + " AND PreEstType='" + PreEstType + "');";

            sql += " select @CoordFee as CoordFee,@TestFee as TestFee,@MeterialFee as MeterialFee";
            sql += ",@EquipFee as EquipFee,@CivilPrice as CivilPrice,@InstallFee as InstallFee";

            sql += " end";


            Dictionary<string, object> map = new Dictionary<string, object>();
            string[] field = new string[] { "CoordFee", "TestFee", "MeterialFee", "EquipFee", "CivilPrice", "InstallFee" };

            map = dao.GetList(sql, field)[0];

            return map;
        }

        /// <summary>
        /// 主业前期测算汇总表 修改
        /// </summary>
        /// <returns></returns>
        public ActionResult PEMainlyUpdate()
        {
            string FieldName = Request.Form["FieldName"] == null ? null : Request.Form["FieldName"].ToString().Replace(" ", "");
            string FieldValue = Request.Form["FieldValue"] == null ? null : Request.Form["FieldValue"].ToString().Replace(" ", "");
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string PreEstType = Request.Form["PreEstType"] == null ? null : Request.Form["PreEstType"].ToString().Replace(" ", "");
            string sql = "update o_PreEstMainly set " + FieldName + "='" + FieldValue + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                var map1 = GetPEMainlySummary(PreEstId, PreEstType);
                sql = "select * from o_PreEstMainly where PreEstId=" + PreEstId;
                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "TZZE", "SJF", "Memo_SJF","SJF_F","JLF","Memo_JLF","JLF_F", "Memo_JGCL", "Memo_JGSB", "JHTJSGF","JHTJSGF_F","Memo_JHTJSGF", "JHAZSGF_F","JHAZSGF", "Memo_JHAZSGF","JHZC_F","JHZC","Memo_JHZC"
                    ,"JHSYF_F","JHSYF","Memo_JHSYF","JHBJSGF","JHBJSGF_F","Memo_JHBJSGF","JHPCF_F","JHPCF","Memo_JHPCF",
                "Memo_CBTJSGF", "Memo_CBAZSGF", "CBZC", "Memo_CBZC", "Memo_CBSYF", "CBBJSGF", "Memo_CBBJSGF", "CBPCF","Memo_CBPCF","Memo_CBXTF","CBSJ","Memo_CBSJ","CBGSGLF","Memo_CBGSGLF","Memo_HJ","Memo_MLR" };
                map2 = dao.GetList(sql, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.DataMap = map1;
                //ViewBag.DataMap = map1;

                result = result.Concat(map1).ToDictionary(k => k.Key, v => v.Value);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        /// <summary>
        /// 主业前期测算——结算 汇总表 修改
        /// </summary>
        /// <returns></returns>
        public ActionResult PEMainlyJSUpdate()
        {
            string FieldName = Request.Form["FieldName"] == null ? null : Request.Form["FieldName"].ToString().Replace(" ", "");
            string FieldValue = Request.Form["FieldValue"] == null ? null : Request.Form["FieldValue"].ToString().Replace(" ", "");
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string PreEstType = Request.Form["PreEstType"] == null ? null : Request.Form["PreEstType"].ToString().Replace(" ", "");
            string sql = "update o_PreEstMainlyJS set " + FieldName + "='" + FieldValue + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                var map1 = GetPEMainlySummary(PreEstId, PreEstType);
                sql = "select * from o_PreEstMainlyJS where PreEstId=" + PreEstId;
                Dictionary<string, object> map2 = new Dictionary<string, object>();
                string[] field = new string[] { "TZZE", "SJF", "Memo_SJF","SJF_F","JLF","Memo_JLF","JLF_F", "Memo_JGCL", "Memo_JGSB", "JHTJSGF","JHTJSGF_F","Memo_JHTJSGF", "JHAZSGF_F","JHAZSGF", "Memo_JHAZSGF","JHZC_F","JHZC","Memo_JHZC"
                    ,"JHSYF_F","JHSYF","Memo_JHSYF","JHBJSGF","JHBJSGF_F","Memo_JHBJSGF","JHPCF_F","JHPCF","Memo_JHPCF",
                "Memo_CBTJSGF", "Memo_CBAZSGF", "CBZC", "Memo_CBZC", "Memo_CBSYF", "CBBJSGF", "Memo_CBBJSGF", "CBPCF","Memo_CBPCF","Memo_CBXTF","CBSJ","Memo_CBSJ","CBGSGLF","Memo_CBGSGLF","Memo_HJ","Memo_MLR" };
                map2 = dao.GetList(sql, field)[0];
                map1 = map1.Concat(map2).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.DataMap = map1;
                //ViewBag.DataMap = map1;

                result = result.Concat(map1).ToDictionary(k => k.Key, v => v.Value);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }





        public ActionResult EstUserCopy()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string ProjectId = Request.Form["ProjectId"] == null ? null : Request.Form["ProjectId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                //复制前期测算表
                string sql = "BEGIN";
                sql += " DECLARE @tempid int,@tempEstId int;";
                sql += " SET @tempEstId="+PreEstId+";";
                sql += " insert INTO o_PreEstUser (Name,ProjectId,CreateTime,IsDelete,Type,XMMC,YDXZ,SSQY,JZMJ,RL,WXCD,HXNBJJE,";
                sql += " Memo_WXTJ,Memo_WXSB,Memo_WXCL,Memo_KBSSB,Memo_KBSCL,Memo_GYSB,Memo_GYCL,Memo_ZYSB,Memo_ZYCL,";
                sql += " Memo_ZTAZ,Memo_ZTTJSGF,Memo_ZTSYTSF,YZYQ_CF,Memo_YZYQCF,YZYQ_YHYB,Memo_YZYQYHYB,YZYQ_LLLJ,Memo_YZYQLLLJ,";
                sql += " YZYQ_CDZ,Memo_YZYQCDZ,DJHTJE,Memo_DJHTJE,SJF,Memo_SJF,JLF,Memo_JLF,DJGLF,Memo_DJGLF,SJ,Memo_SJ,ZJCB,Memo_ZJCB,"; 
                sql += " GSGLF,Memo_GSGLF,Memo_YWF,LRL,Memo_LRL,LPYT,GPRL,ZPRL) ";
                sql += " SELECT '"+ Name + "',ProjectId,CreateTime,IsDelete,Type,XMMC,YDXZ,SSQY,JZMJ,RL,WXCD,HXNBJJE,";
                sql += " Memo_WXTJ,Memo_WXSB,Memo_WXCL,Memo_KBSSB,Memo_KBSCL,Memo_GYSB,Memo_GYCL,Memo_ZYSB,Memo_ZYCL,";
                sql += " Memo_ZTAZ,Memo_ZTTJSGF,Memo_ZTSYTSF,YZYQ_CF,Memo_YZYQCF,YZYQ_YHYB,Memo_YZYQYHYB,YZYQ_LLLJ,Memo_YZYQLLLJ,";
                sql += " YZYQ_CDZ,Memo_YZYQCDZ,DJHTJE,Memo_DJHTJE,SJF,Memo_SJF,JLF,Memo_JLF,DJGLF,Memo_DJGLF,SJ,Memo_SJ,ZJCB,Memo_ZJCB,";
                sql += " GSGLF,Memo_GSGLF,Memo_YWF,LRL,Memo_LRL,LPYT,GPRL,ZPRL FROM o_PreEstUser where PreEstId=@tempEstId;";

                sql += " SET @tempid=(SELECT top 1 PreEstId from o_PreEstUser ORDER BY PreEstId DESC);";

                sql += " insert INTO o_PECoordFee (CooContent,CooDepart,money,Memo,IsDelete,PreEstId,PreEstType,Quantity,QuoteMoney) ";
                sql += " SELECT CooContent,CooDepart,money,Memo,IsDelete,@tempid,PreEstType,Quantity,QuoteMoney";
                sql += " FROM o_PECoordFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PETestFee (TestName,money,IsDelete,Memo,PreEstId,Quantity,QuoteMoney,PreEstType)";
                sql += " SELECT TestName,money,IsDelete,Memo,@tempid,Quantity,QuoteMoney,PreEstType "; 
                sql += " FROM o_PETestFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PEMaterialFeeUser (EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,PreEstId,";
                sql += " EquipPriceId,TabType,PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity)";
                sql += " SELECT EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,@tempid,EquipPriceId,";
                sql += " TabType,PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity";
                sql += " FROM o_PEMaterialFeeUser where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PEInstallFee (Name,Specification,Content,Unit,Money,Memo,IsDelete,PreEstId,Quantity,QuoteMoney,PreEstType,TabType,DesignQuantity) ";
                sql += " SELECT Name,Specification,Content,Unit,Money,Memo,IsDelete,@tempid,Quantity,QuoteMoney,PreEstType,TabType,DesignQuantity";
                sql += " FROM o_PEInstallFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";

                sql += " INSERT INTO o_PEEquipFee (EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,PreEstId,EquipPriceId,TabType,";
                sql += " PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity) ";
                sql += " SELECT EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,@tempid,EquipPriceId,TabType,PreEstType,Quantity,";
                sql += " QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity";
                sql += " FROM o_PEEquipFee where PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";


                sql += " DECLARE @tempCivilId int,@tempCivilAddId int;";
                sql += " DECLARE mycursor CURSOR FOR SELECT PECivilPriceId FROM o_PECivilPrice WHERE PreEstId=@tempEstId AND PreEstType='用户工程' and IsDelete<>1;";
                sql += " OPEN mycursor";
                sql += " FETCH NEXT FROM mycursor INTO @tempCivilId";
                sql += " WHILE (@@fetch_status = 0)";
                sql += " BEGIN";

                sql += " INSERT INTO o_PECivilPrice (Name,TaxRate,Unit,UnitPrice,Memo,IsDelete,LogTime,PreEstId,PreEstType,TabType,OrgCivilPriceId,Quantity) ";
                sql += " SELECT Name,TaxRate,Unit,UnitPrice,Memo,IsDelete,LogTime,@tempid,PreEstType,TabType,OrgCivilPriceId,Quantity";
                sql += " FROM o_PECivilPrice where PECivilPriceId=@tempCivilId;";

                sql += " SET @tempCivilAddId=(SELECT top 1 PECivilPriceId FROM o_PECivilPrice ORDER BY PECivilPriceId DESC);";

                sql += " INSERT INTO o_PECivilPriceSingle (XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,PECivilPriceId,Memo) ";
                sql += " SELECT XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,@tempCivilAddId,Memo FROM o_PECivilPriceSingle";
                sql += " WHERE PECivilPriceId=@tempCivilId;";
                sql += " FETCH NEXT FROM mycursor INTO @tempCivilId;";

                sql += " END";
                sql += " CLOSE mycursor";
                sql += " DEALLOCATE mycursor";

                sql += " END";
                sql += " ";

                BaseDao.execute(sql);


                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {

                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }


        }

        public ActionResult EstUserCSCopy()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string ProjectId = Request.Form["ProjectId"] == null ? null : Request.Form["ProjectId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                //复制前期测算表
                string sql = "BEGIN";
                sql += " DECLARE @tempid int,@tempEstId int;";
                sql += " SET @tempEstId=" + PreEstId + ";";
                sql += " insert INTO o_PreEstUserCS (Name,ProjectId,CreateTime,IsDelete,Type,XMMC,YDXZ,SSQY,JZMJ,RL,WXCD,HXNBJJE,";
                sql += " Memo_WXTJ,Memo_WXSB,Memo_WXCL,Memo_KBSSB,Memo_KBSCL,Memo_GYSB,Memo_GYCL,Memo_ZYSB,Memo_ZYCL,";
                sql += " Memo_ZTAZ,Memo_ZTTJSGF,Memo_ZTSYTSF,YZYQ_CF,Memo_YZYQCF,YZYQ_YHYB,Memo_YZYQYHYB,YZYQ_LLLJ,Memo_YZYQLLLJ,";
                sql += " YZYQ_CDZ,Memo_YZYQCDZ,DJHTJE,Memo_DJHTJE,SJF,Memo_SJF,JLF,Memo_JLF,DJGLF,Memo_DJGLF,SJ,Memo_SJ,ZJCB,Memo_ZJCB,";
                sql += " GSGLF,Memo_GSGLF,Memo_YWF,LRL,Memo_LRL,HTJE,PreEstUserId,LPYT,GPRL,ZPRL) ";
                sql += " SELECT '" + Name + "',ProjectId,CreateTime,IsDelete,Type,XMMC,YDXZ,SSQY,JZMJ,RL,WXCD,HXNBJJE,";
                sql += " Memo_WXTJ,Memo_WXSB,Memo_WXCL,Memo_KBSSB,Memo_KBSCL,Memo_GYSB,Memo_GYCL,Memo_ZYSB,Memo_ZYCL,";
                sql += " Memo_ZTAZ,Memo_ZTTJSGF,Memo_ZTSYTSF,YZYQ_CF,Memo_YZYQCF,YZYQ_YHYB,Memo_YZYQYHYB,YZYQ_LLLJ,Memo_YZYQLLLJ,";
                sql += " YZYQ_CDZ,Memo_YZYQCDZ,DJHTJE,Memo_DJHTJE,SJF,Memo_SJF,JLF,Memo_JLF,DJGLF,Memo_DJGLF,SJ,Memo_SJ,ZJCB,Memo_ZJCB,";
                sql += " GSGLF,Memo_GSGLF,Memo_YWF,LRL,Memo_LRL,HTJE,PreEstUserId,LPYT,GPRL,ZPRL FROM o_PreEstUserCS where PreEstId=@tempEstId;";

                sql += " SET @tempid=(SELECT top 1 PreEstId from o_PreEstUserCS ORDER BY PreEstId DESC);";

                sql += " insert INTO o_PECoordFee (CooContent,CooDepart,money,Memo,IsDelete,PreEstId,PreEstType,Quantity,QuoteMoney) ";
                sql += " SELECT CooContent,CooDepart,money,Memo,IsDelete,@tempid,PreEstType,Quantity,QuoteMoney";
                sql += " FROM o_PECoordFee where PreEstId=@tempEstId AND PreEstType='用户工程前期测算' and IsDelete<>1;";

                sql += " INSERT INTO o_PETestFee (TestName,money,IsDelete,Memo,PreEstId,Quantity,QuoteMoney,PreEstType)";
                sql += " SELECT TestName,money,IsDelete,Memo,@tempid,Quantity,QuoteMoney,PreEstType ";
                sql += " FROM o_PETestFee where PreEstId=@tempEstId AND PreEstType='用户工程前期测算' and IsDelete<>1;";

                sql += " INSERT INTO o_PEMaterialFeeUser (EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,PreEstId,";
                sql += " EquipPriceId,TabType,PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity)";
                sql += " SELECT EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,@tempid,EquipPriceId,";
                sql += " TabType,PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity";
                sql += " FROM o_PEMaterialFeeUser where PreEstId=@tempEstId AND PreEstType='用户工程前期测算' and IsDelete<>1;";

                sql += " INSERT INTO o_PEInstallFee (Name,Specification,Content,Unit,Money,Memo,IsDelete,PreEstId,Quantity,QuoteMoney,PreEstType,TabType,DesignQuantity) ";
                sql += " SELECT Name,Specification,Content,Unit,Money,Memo,IsDelete,@tempid,Quantity,QuoteMoney,PreEstType,TabType,DesignQuantity";
                sql += " FROM o_PEInstallFee where PreEstId=@tempEstId AND PreEstType='用户工程前期测算' and IsDelete<>1;";

                sql += " INSERT INTO o_PEEquipFee (EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,PreEstId,EquipPriceId,TabType,";
                sql += " PreEstType,Quantity,QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity) ";
                sql += " SELECT EquipName,Specification,Unit,Type,LogTime,IsDelete,Price,Code,@tempid,EquipPriceId,TabType,PreEstType,Quantity,";
                sql += " QuoteMoney,SumTax,NormalName,NormalPrice,NormalSpeci,TypeL1,DesignQuantity";
                sql += " FROM o_PEEquipFee where PreEstId=@tempEstId AND PreEstType='用户工程前期测算' and IsDelete<>1;";


                sql += " DECLARE @tempCivilId int,@tempCivilAddId int;";
                sql += " DECLARE mycursor CURSOR FOR SELECT PECivilPriceId FROM o_PECivilPrice WHERE PreEstId=@tempEstId AND PreEstType='用户工程前期测算' and IsDelete<>1;";
                sql += " OPEN mycursor";
                sql += " FETCH NEXT FROM mycursor INTO @tempCivilId";
                sql += " WHILE (@@fetch_status = 0)";
                sql += " BEGIN";

                sql += " INSERT INTO o_PECivilPrice (Name,TaxRate,Unit,UnitPrice,Memo,IsDelete,LogTime,PreEstId,PreEstType,TabType,OrgCivilPriceId,Quantity) ";
                sql += " SELECT Name,TaxRate,Unit,UnitPrice,Memo,IsDelete,LogTime,@tempid,PreEstType,TabType,OrgCivilPriceId,Quantity";
                sql += " FROM o_PECivilPrice where PECivilPriceId=@tempCivilId;";

                sql += " SET @tempCivilAddId=(SELECT top 1 PECivilPriceId FROM o_PECivilPrice ORDER BY PECivilPriceId DESC);";

                sql += " INSERT INTO o_PECivilPriceSingle (XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,PECivilPriceId,Memo) ";
                sql += " SELECT XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,@tempCivilAddId,Memo FROM o_PECivilPriceSingle";
                sql += " WHERE PECivilPriceId=@tempCivilId;";
                sql += " FETCH NEXT FROM mycursor INTO @tempCivilId;";

                sql += " END";
                sql += " CLOSE mycursor";
                sql += " DEALLOCATE mycursor";

                sql += " END";
                sql += " ";

                BaseDao.execute(sql);


                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {

                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }


        }


        public ActionResult EstUserNameUpdate()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            string sql = "update o_PreEstUser set Name='"+ Name + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EstUserCSNameUpdate()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            string sql = "update o_PreEstUserCS set Name='" + Name + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EstMainlyNameUpdate()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            string sql = "update o_PreEstMainly set Name='" + Name + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EstMainlyJSNameUpdate()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");

            string sql = "update o_PreEstMainlyJS set Name='" + Name + "' where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult EstUserDel()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string sql = "update o_PreEstUser set IsDelete=1 where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EstUserCSDel()
        {
            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string sql = "update o_PreEstUserCS set IsDelete=1 where PreEstId=" + PreEstId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EstUserExport()
        {

            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");
            string ProjectName = Request.Form["ProjectName"] == null ? null : Request.Form["ProjectName"].ToString().Replace(" ", "");

            ExcelTool export = new ExcelTool();
            XSSFWorkbook book = export.EstUserExport(PreEstId, ProjectName,Name);
            var ms = new NpoiMemoryStream();
            ms.AllowClose = false;
            book.Write(ms);
            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Name + ".xlsx");

        }

        public ActionResult EstUserCSExport()
        {

            string PreEstId = Request.Form["PreEstId"] == null ? null : Request.Form["PreEstId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");
            string ProjectName = Request.Form["ProjectName"] == null ? null : Request.Form["ProjectName"].ToString().Replace(" ", "");

            ExcelEstUserCS export = new ExcelEstUserCS();
            XSSFWorkbook book = export.EstUserExport(PreEstId, ProjectName, Name);
            var ms = new NpoiMemoryStream();
            ms.AllowClose = false;
            book.Write(ms);
            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Name + ".xlsx");

        }



        public ActionResult EstUserContrastView()
        {
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1 and Type='用户工程'";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;



            return View("EstUserContrastView");
        }


        public ActionResult PEUserConCSView()
        {
            string t1 = Request.QueryString["t1"].ToString();
            string t2 = Request.QueryString["t2"].ToString();
            string t3 = Request.QueryString["t3"].ToString();
            string t4 = Request.QueryString["t4"].ToString();
            string t5 = Request.QueryString["t5"].ToString();

            string PreEstId = "";

            Dictionary<string, object> map1 = new Dictionary<string, object>();
            Dictionary<string, object> map2 = new Dictionary<string, object>();
            Dictionary<string, object> map3 = new Dictionary<string, object>();
            Dictionary<string, object> map4 = new Dictionary<string, object>();
            Dictionary<string, object> map5 = new Dictionary<string, object>();

           // Dictionary<string, object> tempmap = new Dictionary<string, object>();

            if (t1 != "-1")
            {
                
                PreEstId = t1;
                map1 = GetPEUserSummary(PreEstId, "用户工程前期测算");
                string sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                    "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                        "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId","LPYT","GPRL","ZPRL" };
                Dictionary<string, object> tempmap = new Dictionary<string, object>(dao.GetList(sql, field)[0]); ;
                map1 = map1.Concat(tempmap).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.MapCS1 = map1;
            }

            if (t2 != "-1")
            {
                
                PreEstId = t2;
                map2 = GetPEUserSummary(PreEstId, "用户工程前期测算");
                string sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                    "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                        "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId","LPYT","GPRL","ZPRL" };
                Dictionary<string, object> tempmap = new Dictionary<string, object>(dao.GetList(sql, field)[0]); ;
                map2 = map2.Concat(tempmap).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.MapCS2 = map2;
            }
            else
            {
                Dictionary<string, object> tempmap = new Dictionary<string, object>(map1); 
                string[] keys = map1.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    tempmap[keys[i]] = "-";
                }
                ViewBag.MapCS2 = tempmap;
            }

            if (t3 != "-1")
            {

                PreEstId = t3;
                map3 = GetPEUserSummary(PreEstId, "用户工程前期测算");
                string sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                    "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                        "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId","LPYT","GPRL","ZPRL" };
                Dictionary<string, object> tempmap = new Dictionary<string, object>(dao.GetList(sql, field)[0]); ;
                map3 = map3.Concat(tempmap).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.MapCS3 = map3;
            }
            else
            {
                Dictionary<string, object> tempmap = new Dictionary<string, object>(map1);
                string[] keys = map1.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    tempmap[keys[i]] = "-";
                }
                ViewBag.MapCS3 = tempmap;
            }

            if (t4 != "-1")
            {

                PreEstId = t4;
                map4 = GetPEUserSummary(PreEstId, "用户工程前期测算");
                string sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                    "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                        "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId","LPYT","GPRL","ZPRL" };
                Dictionary<string, object> tempmap = new Dictionary<string, object>(dao.GetList(sql, field)[0]); ;
                map4 = map4.Concat(tempmap).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.MapCS4 = map4;
            }
            else
            {
                Dictionary<string, object> tempmap = new Dictionary<string, object>(map1);
                string[] keys = map1.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    tempmap[keys[i]] = "-";
                }
                ViewBag.MapCS4 = tempmap;
            }

            if (t5 != "-1")
            {

                PreEstId = t5;
                map5 = GetPEUserSummary(PreEstId, "用户工程前期测算");
                string sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
                string[] field = new string[] { "XMMC", "YDXZ", "SSQY", "JZMJ", "RL", "WXCD", "HXNBJJE", "Memo_WXTJ","Memo_WXSB","Memo_WXCL","Memo_KBSSB","Memo_KBSCL","Memo_GYSB","Memo_GYCL",
                    "Memo_ZYSB", "Memo_ZYCL", "Memo_ZTAZ", "Memo_ZTTJSGF", "Memo_ZTSYTSF", "YZYQ_CF", "Memo_YZYQCF", "YZYQ_YHYB","Memo_YZYQYHYB","YZYQ_LLLJ","Memo_YZYQLLLJ","YZYQ_CDZ","Memo_YZYQCDZ","DJHTJE","Memo_DJHTJE",
                        "SJF","Memo_SJF","JLF","Memo_JLF","DJGLF","Memo_DJGLF","SJ","Memo_SJ","ZJCB","Memo_ZJCB","GSGLF","Memo_GSGLF","Memo_YWF","LRL","Memo_LRL","HTJE","PreEstUserId","LPYT","GPRL","ZPRL" };
                Dictionary<string, object> tempmap = new Dictionary<string, object>(dao.GetList(sql, field)[0]); ;
                map5 = map5.Concat(tempmap).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.MapCS5 = map5;
            }
            else
            {
                Dictionary<string, object> tempmap = new Dictionary<string, object>(map1);
                string[] keys = map1.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    tempmap[keys[i]] = "-";
                }
                ViewBag.MapCS5 = tempmap;
            }




            return View("PEUserConCSView");
        }

        public ActionResult WorkQuantitiesView()
        {
            string PreEstId = Request.QueryString["PreEstId"].ToString();
            string ProjectId = Request.QueryString["ProjectId"].ToString();
            ViewBag.PreEstUserCSId = PreEstId;
            ViewBag.ProjectId = ProjectId;

            string PreEstUserId = "";
            //查询项目信息
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1 and Type='用户工程'";
            var ProjectInfoMap = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" })[0];
            ViewBag.ProjectInfoMap = ProjectInfoMap;

            //查询对应报价测算表的ID
            sql = "select * from o_PreEstUserCS where PreEstId=" + PreEstId;
            PreEstUserId = dao.GetList(sql, new string[] { "PreEstUserId" })[0]["PreEstUserId"].ToString();
            ViewBag.PreEstUserId = PreEstUserId;


            return View("WorkQuantitiesView");
        }


        public ActionResult WorkQuantitiesTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,WorkQuantitiesId,Type,CostType,PreEstUserId,PreEstUserCSId,Name,BJTZNum,BJSJNum," +
                        " CSTZNum,CSSJNum,SJSFNum,BJMoney,CSMoney,DifferentMemo,PurchaseMemo,GetTimeMemo,ProjectId" +
                        " FROM o_WorkQuantities " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "WorkQuantitiesId", "Type", "CostType", "PreEstUserId", "PreEstUserCSId", "Name", "BJTZNum", "BJSJNum", "CSTZNum", "CSSJNum", "SJSFNum",
                    "BJMoney","CSMoney","DifferentMemo", "PurchaseMemo", "GetTimeMemo", "ProjectId" });
                SqlNum = "select count(WorkQuantitiesId) as TotalNum FROM o_WorkQuantities where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /EstimateController/WorkQuantitiesTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult WorkQuantitiesAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string GetTimeMemo = obj["GetTimeMemo"] == null ? null : obj["GetTimeMemo"].ToString().Replace(" ", "");
            string BJTZNum = obj["BJTZNum"] == null ? null : obj["BJTZNum"].ToString().Replace(" ", "");
            string BJSJNum = obj["BJSJNum"] == null ? null : obj["BJSJNum"].ToString().Replace(" ", "");
            string CSTZNum = obj["CSTZNum"] == null ? null : obj["CSTZNum"].ToString().Replace(" ", "");
            string CSSJNum = obj["CSSJNum"] == null ? null : obj["CSSJNum"].ToString().Replace(" ", "");
            string SJSFNum = obj["SJSFNum"] == null ? null : obj["SJSFNum"].ToString().Replace(" ", "");
            string BJMoney = obj["BJMoney"] == null ? null : obj["BJMoney"].ToString().Replace(" ", "");
            string CSMoney = obj["CSMoney"] == null ? null : obj["CSMoney"].ToString().Replace(" ", "");
            string DifferentMemo = obj["DifferentMemo"] == null ? null : obj["DifferentMemo"].ToString().Replace(" ", "");
            string PurchaseMemo = obj["PurchaseMemo"] == null ? null : obj["PurchaseMemo"].ToString().Replace(" ", "");
            string PreEstUserId = obj["PreEstUserId"] == null ? null : obj["PreEstUserId"].ToString().Replace(" ", "");
            string PreEstUserCSId = obj["PreEstUserCSId"] == null ? null : obj["PreEstUserCSId"].ToString().Replace(" ", "");
            string ProjectId = obj["ProjectId"] == null ? null : obj["ProjectId"].ToString().Replace(" ", "");
            string Type = obj["Type"] == null ? null : obj["Type"].ToString().Replace(" ", "");
            string CostType= obj["CostType"] == null ? null : obj["CostType"].ToString().Replace(" ", "");

            //Content = Content.Replace("\r\n", "\r\n<br>");
            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["GetTimeMemo"] = GetTimeMemo,
                    ["BJTZNum"] = BJTZNum,
                    ["BJSJNum"] = BJSJNum,
                    ["CSTZNum"] = CSTZNum,
                    ["CSSJNum"] = CSSJNum,
                    ["SJSFNum"] = SJSFNum,
                    ["BJMoney"] = BJMoney,
                    ["CSMoney"] = CSMoney,
                    ["DifferentMemo"] = DifferentMemo,
                    ["PurchaseMemo"] = PurchaseMemo,
                    ["PreEstUserId"] = PreEstUserId,
                    ["PreEstUserCSId"] = PreEstUserCSId,
                    ["ProjectId"] = ProjectId,
                    ["Type"] = Type,
                    ["CostType"]= CostType,
                };
                dao.save("o_WorkQuantities", map);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult WorkQuantitiesUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string WorkQuantitiesId = obj["WorkQuantitiesId"] == null ? null : obj["WorkQuantitiesId"].ToString().Replace(" ", "");
            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string BJTZNum = obj["BJTZNum"] == null ? null : obj["BJTZNum"].ToString().Replace(" ", "");
            string BJSJNum = obj["BJSJNum"] == null ? null : obj["BJSJNum"].ToString().Replace(" ", "");
            string CSTZNum = obj["CSTZNum"] == null ? null : obj["CSTZNum"].ToString().Replace(" ", "");
            string CSSJNum = obj["CSSJNum"] == null ? null : obj["CSSJNum"].ToString().Replace(" ", "");
            string SJSFNum = obj["SJSFNum"] == null ? null : obj["SJSFNum"].ToString().Replace(" ", "");
            string BJMoney = obj["BJMoney"] == null ? null : obj["BJMoney"].ToString().Replace(" ", "");
            string CSMoney = obj["CSMoney"] == null ? null : obj["CSMoney"].ToString().Replace(" ", "");
            string DifferentMemo = obj["DifferentMemo"] == null ? null : obj["DifferentMemo"].ToString().Replace(" ", "");
            string PurchaseMemo = obj["PurchaseMemo"] == null ? null : obj["PurchaseMemo"].ToString().Replace(" ", "");
            string GetTimeMemo = obj["GetTimeMemo"] == null ? null : obj["GetTimeMemo"].ToString().Replace(" ", "");


            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["GetTimeMemo"] = GetTimeMemo,
                    ["BJTZNum"] = BJTZNum,
                    ["BJSJNum"] = BJSJNum,
                    ["CSTZNum"] = CSTZNum,
                    ["CSSJNum"] = CSSJNum,
                    ["SJSFNum"] = SJSFNum,
                    ["BJMoney"] = BJMoney,
                    ["CSMoney"] = CSMoney,
                    ["DifferentMemo"] = DifferentMemo,
                    ["PurchaseMemo"] = PurchaseMemo,


                };
                dao.update("o_WorkQuantities", map, "WorkQuantitiesId=" + WorkQuantitiesId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }



        public ActionResult WorkQuantitiesBatchDel()
        {
            string[] Ids = Request.Form.GetValues("WorkQuantitiesId[]");
            string temp = "";
            for (int i = 0; i < Ids.Length; i++)
            {
                temp += Ids[i] + ",";
            }
            temp = temp.Substring(0, temp.Length - 1);
            string sql = "delete o_WorkQuantities  where WorkQuantitiesId in (" + temp + ")";

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult WorkQuantitiesBatchAdd()
        {
            string TabType = Request.Form["TabType"];
            string TypeName = Request.Form["TypeName"];
            string PreEstUserCSId = Request.Form["PreEstUserCSId"];
            string PreEstUserId = Request.Form["PreEstUserId"];
            string ProjectId = Request.Form["ProjectId"];

            string sql = "";

            if(TabType== "TJ" && TypeName == "土建")
            {   //土建
                sql = "Begin " +
                    " SELECT * INTO #temp FROM" +
                    " (" +
                    " SELECT Name as Name,0 as BJTZNum,0 as BJSJNum,0 AS CSTZNum,SUM(Quantity) as CSSJNum,'外线土建' as CostType,0 as BJMoney,SUM(Quantity*UnitPrice) as CSMoney FROM o_PECivilPrice" +
                    " where IsDelete=0 and PreEstType='用户工程前期测算' AND PreEstId=" + PreEstUserCSId + " AND TabType='WX' GROUP BY Name,Quantity" +
                    " UNION" +
                    " SELECT Name as Name,0 AS BJTZNum,SUM(Quantity) as BJSJNum,0 as CSTZNum,0 as CSSJNum,'外线土建' as CostType,SUM(Quantity*UnitPrice) as BJMoney,0 as CSMoney FROM o_PECivilPrice" +
                    " where IsDelete=0 and  PreEstType='用户工程' AND PreEstId=" + PreEstUserId + " AND TabType='WX' GROUP BY Name,Quantity" +

                    " UNION" +
                    " SELECT Name as Name,0 as BJTZNum,0 as BJSJNum,0 AS CSTZNum,SUM(Quantity) as CSSJNum,'外线土建' as CostType,0 as BJMoney,SUM(Quantity*UnitPrice) as CSMoney FROM o_PECivilPrice" +
                    " where IsDelete=0 and PreEstType='用户工程前期测算' AND PreEstId=" + PreEstUserCSId + " AND TabType='HXN' GROUP BY Name,Quantity" +
                    " UNION" +
                    " SELECT Name as Name,0 AS BJTZNum,SUM(Quantity) as BJSJNum,0 as CSTZNum,0 as CSSJNum,'外线土建' as CostType,SUM(Quantity*UnitPrice) as BJMoney,0 as CSMoney FROM o_PECivilPrice" +
                    " where IsDelete=0 and  PreEstType='用户工程' AND PreEstId=" + PreEstUserId + " AND TabType='HXN' GROUP BY Name,Quantity" +

                    ") as a;" +
                    " INSERT INTO o_WorkQuantities(Type,PreEstUserId,PreEstUserCSId,ProjectId,Name,BJTZNum,BJSJNum,CSTZNum,CSSJNum,CostType,BJMoney,CSMoney) " +
                    " select '" + TypeName + "'," + PreEstUserId + "," + PreEstUserCSId + "," + ProjectId + ",Name,SUM(BJTZNum) as BJTZNum, SUM(BJSJNum) as BJSJNum, SUM(CSTZNum) as CSTZNum, SUM(CSSJNum) as CSSJNum," +
                    " CostType,SUM(BJMoney) AS BJMoney,SUM(CSMoney) AS CSMoney FROM #temp GROUP BY  Name,CostType;" +
                    " DROP TABLE #temp;" +
                    " End";
            }
            else 
            {   //设备和材料
                sql = "Begin " +
                    " SELECT * INTO #temp FROM" +
                    " (" +
                    " SELECT TypeL1 as Name,0 as BJTZNum,0 as BJSJNum,SUM(DesignQuantity) AS CSTZNum,SUM(Quantity) as CSSJNum,'设备' as CostType,0 as BJMoney,SUM(Quantity*QuoteMoney) as CSMoney FROM o_PEEquipFee" +
                    " where IsDelete=0 and PreEstType='用户工程前期测算' AND PreEstId=" + PreEstUserCSId + " AND TabType='" + TabType + "' AND TypeL1 is not NULL GROUP BY TypeL1,Quantity" +
                    " UNION" +
                    " SELECT TypeL1 as Name,SUM(DesignQuantity) AS BJTZNum,SUM(Quantity) as BJSJNum,0 as CSTZNum,0 as CSSJNum,'设备' as CostType,SUM(Quantity*QuoteMoney) as BJMoney,0 as CSMoney FROM o_PEEquipFee" +
                    " where IsDelete=0 and  PreEstType='用户工程' AND PreEstId=" + PreEstUserId + " AND TabType='" + TabType + "' AND TypeL1 is not NULL GROUP BY TypeL1,Quantity" +

                    " UNION" +
                    " SELECT TypeL1 as Name,0 as BJTZNum,0 as BJSJNum,SUM(DesignQuantity) AS CSTZNum,SUM(Quantity) as CSSJNum,'材料' as CostType,0 as BJMoney,SUM(Quantity*QuoteMoney) as CSMoney FROM o_PEMaterialFeeUser" +
                    " where IsDelete=0 and  PreEstType='用户工程前期测算' AND PreEstId=" + PreEstUserCSId + " AND TabType='" + TabType + "' AND TypeL1 is not NULL GROUP BY TypeL1,Quantity" +
                    " UNION" +
                    " SELECT TypeL1 as Name,SUM(DesignQuantity) AS BJTZNum,SUM(Quantity) as BJSJNum,0 as CSTZNum,0 as CSSJNum,'材料' as CostType,SUM(Quantity*QuoteMoney) as BJMoney,0 as CSMoney  FROM o_PEMaterialFeeUser " +
                    " where IsDelete=0 and PreEstType='用户工程' AND PreEstId=" + PreEstUserId + " AND TabType='" + TabType + "' AND TypeL1 is not NULL GROUP BY TypeL1,Quantity" +

                    //" UNION" +
                    //" SELECT Name as Name,0 AS BJTZNum,0 as BJSJNum,SUM(DesignQuantity) AS CSTZNum,SUM(Quantity) as CSSJNum,'安装' as CostType,0 as BJMoney,SUM(Quantity*QuoteMoney) as CSMoney  FROM o_PEInstallFee " +
                    //" where IsDelete=0 and PreEstType='用户工程前期测算' AND PreEstId=" + PreEstUserCSId + " AND TabType='" + TabType + "' GROUP BY Name,Quantity" +
                    //" UNION" +
                    //" SELECT Name as Name,SUM(DesignQuantity) AS BJTZNum,SUM(Quantity) as BJSJNum,0 as CSTZNum,0 as CSSJNum,'安装' as CostType,SUM(Quantity*QuoteMoney) as BJMoney,0 as CSMoney  FROM o_PEInstallFee " +
                    //" where IsDelete=0 and PreEstType='用户工程' AND PreEstId=" + PreEstUserId + " AND TabType='" + TabType + "' GROUP BY Name,Quantity" +
                    ") as a;" +
                    " INSERT INTO o_WorkQuantities(Type,PreEstUserId,PreEstUserCSId,ProjectId,Name,BJTZNum,BJSJNum,CSTZNum,CSSJNum,CostType,BJMoney,CSMoney) " +
                    " select '" + TypeName + "'," + PreEstUserId + "," + PreEstUserCSId + "," + ProjectId + ",Name,SUM(BJTZNum) as BJTZNum, SUM(BJSJNum) as BJSJNum, SUM(CSTZNum) as CSTZNum, SUM(CSSJNum) as CSSJNum," +
                    " CostType,SUM(BJMoney) AS BJMoney,SUM(CSMoney) AS CSMoney FROM #temp GROUP BY  Name,CostType;" +
                    " DROP TABLE #temp;" +
                    " End";
            }


            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }






        }



    }
}