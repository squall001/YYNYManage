﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YYNY.Tools;
using YYNY.Tools.Dao;
using System.IO;
using Newtonsoft.Json.Linq;

namespace YYNY.Controllers
{
    public class InventoryController : Controller
    {
        BaseDao dao = new BaseDao();
        // GET: Inventory
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InventoryView()
        {
            //获取项目列表
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;
            
            return View();
        }

        public ActionResult InveTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,InventoryId,ProjectId,Code,Specification,Unit,RequAmount,ReceAmount,CONVERT(varchar(100),ReceDate,23) as ReceDate" +
                        " ,SubTakeAmount,CONVERT(varchar(100),SubRecedate,23) as SubRecedate,SubContact,CONVERT(varchar(100),SubRefuDate,23) as SubRefuDate,SubUseAmount,SettAmount,RemaAmount,MaterialMemo,InvenAddressId,DATEDIFF(SS,'1970-1-1 00:00:00',OpTime) as OpTime" +
                        " FROM t_d_Inventory " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "InventoryId", "ProjectId", "Code", "Specification", "Unit", "RequAmount", "ReceAmount", "ReceDate", "SubTakeAmount", "SubRecedate", "SubContact", "SubRefuDate", "SubUseAmount", "SettAmount", "RemaAmount", "MaterialMemo", "InvenAddressId","OpTime" });
                SqlNum = "select count(ProjectId) as TotalNum FROM t_d_Inventory where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /ProjectController/InfoTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult inveAddView()
        {
            string ProjectId = Request.QueryString["ProjectId"].ToString();
            ViewBag.ProjectId = ProjectId;
            return View();
        }

        public ActionResult InveAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string ProjectId = obj["ProjectId"] == null ? null : obj["ProjectId"].ToString().Replace(" ", "");
            string Code = obj["Code"] == null ? null : obj["Code"].ToString().Replace(" ", "");
            string Specification = obj["Specification"] == null ? null : obj["Specification"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string RequAmount = obj["RequAmount"] == null ? null : obj["RequAmount"].ToString().Replace(" ", "");
            string ReceAmount = obj["ReceAmount"] == null ? null : obj["ReceAmount"].ToString().Replace(" ", "");
            string ReceDate = obj["ReceDate"] == null ? null : obj["ReceDate"].ToString().Replace(" ", "");
            string SubTakeAmount = obj["SubSendAmount"] == null ? null : obj["SubSendAmount"].ToString().Replace(" ", "");
            string SubRecedate = obj["SubRecedate"] == null ? null : obj["SubRecedate"].ToString().Replace(" ", "");
            string SubContact = obj["Contact"] == null ? null : obj["Contact"].ToString().Replace(" ", "");
            //string SubRefuAmount = obj["SubRefuAmount"] == null ? null : obj["SubRefuAmount"].ToString().Replace(" ", "");
            string SubRefuDate = obj["SubRefuDate"] == null ? null : obj["SubRefuDate"].ToString().Replace(" ", "");
            string SubUseAmount = obj["SubUseAmount"] == null ? null : obj["SubUseAmount"].ToString().Replace(" ", "");
            //string SubRefuAmount = obj["SubRefuAmount"] == null ? null : obj["SubRefuAmount"].ToString().Replace(" ", "");
            string SettAmount = obj["SettAmount"] == null ? null : obj["SettAmount"].ToString().Replace(" ", "");
            string RemaAmount = obj["RemaAmount"] == null ? null : obj["RemaAmount"].ToString().Replace(" ", "");
            string MaterialMemo = obj["MaterialMemo"] == null ? null : obj["MaterialMemo"].ToString().Replace(" ", "");


            if (ReceDate == "")
            {
                ReceDate = null;
            }
            if (SubRecedate == "")
            {
                SubRecedate = null;
            }
            if (SubRefuDate == "")
            {
                SubRefuDate = null;
            }



            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["ProjectId"] = ProjectId,
                    ["Code"] = Code,
                    ["Specification"] = Specification,
                    ["Unit"] = Unit,
                    ["RequAmount"] = RequAmount,
                    ["ReceAmount"] = ReceAmount,
                    ["ReceDate"] = ReceDate,
                    ["SubTakeAmount"] = SubTakeAmount,
                    ["SubRecedate"] = SubRecedate,
                    ["SubContact"] = SubContact,
                    ["SubRefuDate"] = SubRefuDate,
                    ["SubUseAmount"] = SubUseAmount,
                    ["SettAmount"] = SettAmount,
                    ["RemaAmount"] = RemaAmount,
                    ["MaterialMemo"] = MaterialMemo,
                    ["IsDelete"] = 0,
                };
                
                //foreach (var item in obj)
                //{
                //    ReceDate = null;
                //}

                dao.save("t_d_Inventory", map);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult InveEditView()
        {

            string InventoryId = Request.QueryString["InventoryId"].ToString();

            string sql = "select * from t_d_inventory where inventoryid=" + InventoryId;
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            list = dao.GetList(sql, new string[] { "InventoryId", "ProjectId", "Code", "Specification", "Unit", "RequAmount", "ReceAmount", "ReceDate", "SubTakeAmount", "SubRecedate", "SubContact", "SubRefuDate", "SubUseAmount", "SettAmount", "RemaAmount", "MaterialMemo" });
            ViewBag.InventoryId = InventoryId;
            ViewBag.Map = list[0];

            return View();

        }

        public ActionResult InveEdit()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>();

                foreach (var item in obj)
                {
                    if ("ReceDate" == item.Key || "SubRecedate" == item.Key || "SubRefuDate" == item.Key)
                    {
                        if (item.Value.ToString() == "")
                        {
                            map.Add(item.Key, null);
                        }
                        else
                        {
                            map.Add(item.Key, item.Value);
                        }                   
                    }
                    else
                    {
                        map.Add(item.Key, item.Value);
                    }
                }
                string InventoryId = map["InventoryId"].ToString();
                map.Remove("InventoryId");

                dao.update("t_d_Inventory", map, " InventoryId="+ InventoryId);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult InveDel()
        { 
            string InventoryId = Request.Form["InventoryId"] == null ? null : Request.Form["InventoryId"].ToString().Replace(" ", "");
            string OpTime= Request.Form["OpTime"] == null ? null : Request.Form["OpTime"].ToString().Replace(" ", "");
            

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                string sql = "update t_d_Inventory set IsDelete=1 where InventoryId=" + InventoryId;
                BaseDao.execute(sql);

                sql = "update t_o_InvenRequisition set IsDelete=1 where opTime=(DATEADD(SS, "+ OpTime+", '1970-1-1 00:00:00'))";
                BaseDao.execute(sql);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
            
        }

        public ActionResult RefuUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>();

                foreach (var item in obj)
                {

                    map.Add(item.Key, item.Value);
                    
                }
                string InventoryId = map["InventoryId"].ToString();
                map.Remove("InventoryId");

                dao.update("t_d_Inventory", map, " InventoryId=" + InventoryId);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult Upload()
        {


            string tpye = Request.Files[0].ContentType;
            string UpName = Request.Files[0].FileName;
            //获取后缀名
            string exName = UpName.Substring(UpName.LastIndexOf(".") + 1, (UpName.Length - UpName.LastIndexOf(".") - 1));
            string fileName = "File" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + exName;
            if (Directory.Exists(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd"))) == false)//如果不存在就创建file文件夹
            {
                Directory.CreateDirectory(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd")));
            }
            string filePhysicalPath = Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd") + "/" + fileName);
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                if (Request.Form["ProjectId"] == "")
                {
                    result.Add("result", "没有选择主项目");
                    return Json(result);
                }

                Request.Files[0].SaveAs(filePhysicalPath);

                string ErrorMsg = "";
                ErrorMsg = ExcelTool.ImportInveAnalysis(filePhysicalPath, Request.Form["ProjectId"].ToString());
                if (ErrorMsg == "OK")
                {
                    result.Add("result", "OK");
                    return Json(result);
                }
                else
                {
                    result.Add("result", ErrorMsg);
                    return Json(result);
                }


                //upImg.SaveAs(filePhysicalPath);
                //pic = "/upload/" + fileName;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult StorageView()
        {
            //获取项目列表
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;

            return View();
        }

        public ActionResult InvenRequisition()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>();

                foreach (var item in obj)
                {

                    map.Add(item.Key, item.Value);

                }
                string TempTime = DateTime.Now.ToString();
                map.Add("IsDelete", 0);
                map.Add("OpTime", TempTime);
                dao.save("t_o_InvenRequisition", map);

                //在对应项目内增加工程材料登记表

                string sql = "select a.Code,a.Specification,a.Unit,b.Name from t_d_Inventory a inner join t_d_ProjectInfo b on a.projectId=b.projectId" +
                    " where InventoryId=" + map["InventoryId"].ToString();
                var InvenMap = dao.GetList(sql, new string[] { "Code", "Specification", "Unit","Name" })[0];
                InvenMap.Add("IsDelete", 0);
                InvenMap.Add("OpTime", TempTime);
                InvenMap.Add("ProjectId", map["TargProjId"].ToString());
                InvenMap.Add("ReceAmount", map["RequiAmount"].ToString());
                InvenMap.Add("MaterialMemo", "调入项目名："+InvenMap["Name"].ToString());
                InvenMap.Remove("Name");

                dao.save("t_d_Inventory", InvenMap);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult StorageTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,a.InventoryId,a.InvenAddressId,a.Code,a.Specification,a.Unit,c.Name as ProjName," +
                        " CASE WHEN a.RemaAmount-SUM(b.RequiAmount) IS NULL THEN a.RemaAmount ELSE a.RemaAmount-SUM(b.RequiAmount) END as storNum" +
                        " from t_d_Inventory a LEFT JOIN t_o_InvenRequisition b on a.InventoryId=b.InventoryId and b.IsDelete<>1" +
                        " LEFT JOIN t_d_ProjectInfo c ON a.ProjectId=c.projectId"+
                        " where a.IsDelete<>1 AND a.RemaAmount>0 " + condition +
                        " GROUP BY a.InventoryId,a.InvenAddressId,a.Code,a.Specification,a.Unit,a.RemaAmount,c.Name" +
                        " HAVING (a.RemaAmount-SUM(b.RequiAmount))>0 OR SUM(b.RequiAmount) is NULL" +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "InventoryId", "InvenAddressId", "Code", "Specification", "Unit", "ProjName", "storNum" });
                SqlNum = "select count(InventoryId) as TotalNum FROM ( " +
                    " SELECT a.InventoryId" +
                    " from t_d_Inventory a LEFT JOIN t_o_InvenRequisition b on a.InventoryId=b.InventoryId and b.IsDelete<>1" +
                    " LEFT JOIN t_d_ProjectInfo c ON a.ProjectId=c.projectId" +
                    " where a.IsDelete<>1 AND a.RemaAmount>0" + condition +
                    " GROUP BY a.InventoryId,a.InvenAddressId,a.Code,a.Specification,a.Unit,a.RemaAmount,c.Name" +
                    " HAVING (a.RemaAmount-SUM(b.RequiAmount))>0 OR SUM(b.RequiAmount) is NULL" +
                    ") as s";

                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /InventoryController/InfoTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }
        }

        public ActionResult RequisitionView()
        {
            //获取项目列表
            string sql = "select ProjectId,Name,ProjectCode,Type from t_d_ProjectInfo where IsDelete<>1";
            var ProjectList = dao.GetList(sql, new string[] { "ProjectId", "Name", "ProjectCode", "Type" });
            ViewBag.ProjectList = ProjectList;
            return View();
        }

        public ActionResult RequisitionTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,a.InvenRequisitionID,a.InventoryId,a.SingPrice,a.RequiAmount,CONVERT(varchar(100),a.RequiTime,23) as RequiTime,b.Code,b.Specification,b.Unit," +
                        " c.Name as InProjName,e.Name as OriAdress,d.Name as OutProjName" +
                        " from t_o_InvenRequisition a INNER JOIN t_d_Inventory b on a.InventoryId=b.InventoryId " +
                        " INNER JOIN t_d_ProjectInfo c on b.ProjectId=c.projectId" +
                        " INNER JOIN t_d_ProjectInfo d ON a.TargProjId=d.projectId" +
                        " INNER JOIN t_d_InvenAddress e ON b.InvenAddressId=e.InvenAddressId" +
                        " where a.IsDelete<>1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "InvenRequisitionID", "InventoryId", "SingPrice", "RequiAmount", "RequiTime", "Code", "Specification", "Unit", "InProjName", "OriAdress", "OutProjName" });
                SqlNum = "select count(InvenRequisitionID) as TotalNum" +
                    " from t_o_InvenRequisition a INNER JOIN t_d_Inventory b on a.InventoryId=b.InventoryId" +
                    " INNER JOIN t_d_ProjectInfo c on b.ProjectId=c.projectId" +
                    " INNER JOIN t_d_ProjectInfo d ON a.TargProjId=d.projectId" +
                    " INNER JOIN t_d_InvenAddress e ON b.InvenAddressId=e.InvenAddressId" +
                    " where a.IsDelete<>1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /InventoryController/RequisitionTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult MaterialView()
        {
            return View();
        }


        public ActionResult MateTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,MaterialId,Type,Code,Specification,Unit,RecoPrice,CONVERT(varchar(100),LogTime,23) as LogTime,Weight" +
                        " FROM t_d_Material " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "MaterialId", "Type", "Code", "Specification", "Unit", "RecoPrice", "LogTime", "Weight" });
                SqlNum = "select count(MaterialId) as TotalNum FROM t_d_Material where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /InventoryController/MateTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult MateUpload()
        {


            string tpye = Request.Files[0].ContentType;
            string UpName = Request.Files[0].FileName;
            //获取后缀名
            string exName = UpName.Substring(UpName.LastIndexOf(".") + 1, (UpName.Length - UpName.LastIndexOf(".") - 1));
            string fileName = "File" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + exName;
            if (Directory.Exists(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd"))) == false)//如果不存在就创建file文件夹
            {
                Directory.CreateDirectory(Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd")));
            }
            string filePhysicalPath = Server.MapPath("~/upload/" + DateTime.Now.ToString("yyyyMMdd") + "/" + fileName);
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {


                Request.Files[0].SaveAs(filePhysicalPath);

                string ErrorMsg = "";
                ErrorMsg = ExcelTool.ImportMateAnalysis(filePhysicalPath);
                if (ErrorMsg == "OK")
                {
                    result.Add("result", "OK");
                    return Json(result);
                }
                else
                {
                    result.Add("result", ErrorMsg);
                    return Json(result);
                }


                //upImg.SaveAs(filePhysicalPath);
                //pic = "/upload/" + fileName;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult MaterialEdit()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>();

                foreach (var item in obj)
                {
                        map.Add(item.Key, item.Value); 
                }
                string MaterialId = map["MaterialId"].ToString();
                map.Remove("MaterialId");

                dao.update("t_d_Material", map, " MaterialId=" + MaterialId);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult MaterialDel()
        {
            string MaterialId = Request.Form["MaterialId"] == null ? null : Request.Form["MaterialId"].ToString().Replace(" ", "");



            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                string sql = "update t_d_Material set IsDelete=1 where MaterialId=" + MaterialId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }


    }
}