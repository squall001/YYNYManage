﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YYNY.Tools;
using YYNY.Tools.Dao;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace YYNY.Controllers
{
    public class AuthorityController : Controller
    {
        // GET: Authority

        BaseDao dao = new BaseDao();

        public ActionResult AuthorityView()
        {
            string UserId = Request.QueryString["UserId"] ?? null;
            string sql = "select * from o_Authority where UserId=" + UserId;

            try
            {
                var AuthList = dao.GetList(sql, new string[] { "AuthorityId", "Type", "Name", "UserId" });
                ViewBag.AuthList = AuthList;
                ViewBag.UserId = UserId;

                return View("AuthorityView");

            }
            catch (Exception e)
            {
                return Content("错误");

            }

        }

        /// <summary>
        /// 接收权限保存功能
        /// 其中必然传上来一个数据，也就是读取后的list.Count>=1
        /// </summary>
        /// <returns></returns>
        public ActionResult AuthSave()
        {

            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            var list = new List<Dictionary<string, object>>();

            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            list = Serializer.Deserialize<List<Dictionary<string, object>>>(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            bool NoAuth = false;
            if(list.Count ==1)
            {
                if(list[0]["Type"].ToString()== "NoAuth")
                {
                    NoAuth = true;
                }
            }


            try
            {
                string sql = "begin ";
                for (int i = 0; i < list.Count; i++)
                {
                    sql += "insert into o_Authority (Type,Name,UserId) values('" + list[i]["Type"].ToString() + "','" + list[i]["Name"].ToString() + "','" +
                        list[i]["UserId"].ToString() + "')";
                }
                sql += " end";

                string sql2 = "delete o_Authority where UserId=" + list[0]["UserId"].ToString();
                if (NoAuth == true)
                {
                    BaseDao.execute(sql2);
                }
                else
                {
                    BaseDao.execute(sql2);
                    BaseDao.execute(sql);

                }

                //string temp = dao.GetString("o_Authority", "Name", "UserId=16");
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }


        }






    }
}