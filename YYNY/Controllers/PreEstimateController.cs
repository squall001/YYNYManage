﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YYNY.Tools;
using YYNY.Tools.Dao;
using System.IO;
using Newtonsoft.Json.Linq;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace YYNY.Controllers
{
    public class PreEstimateController : Controller
    {
        BaseDao dao = new BaseDao();
        // GET: PreEstimate
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 核价单
        /// </summary>
        /// <returns></returns>
        public ActionResult CivilPriceSingle()
        {
            return View("CivilPriceSingleView");
        }

        public ActionResult CivilPrice()
        {
            return View("CivilPriceView");
        }

        public ActionResult CivilPriceSingleEdit()
        {
            string CivilPriceId = Request.QueryString["CivilPriceId"].ToString();
            string sql = "select * from t_d_CivilPrice where CivilPriceId=" + CivilPriceId;
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            list = dao.GetList(sql, new string[] { "CivilPriceId", "Name", "TaxRate", "Unit", "UnitPrice", "Memo" });

            ViewBag.CivilPriceId = CivilPriceId;
            ViewBag.Name = list[0]["Name"].ToString();
            ViewBag.TaxRate = list[0]["TaxRate"].ToString();
            ViewBag.Unit = list[0]["Unit"].ToString();
            ViewBag.UnitPrice = list[0]["UnitPrice"].ToString();
            ViewBag.Memo = list[0]["Memo"].ToString();


            return View("CivilPriceSingleEditView");
        }

        public ActionResult CivilTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,CivilPriceId,Name,TaxRate,Unit,UnitPrice,Memo" +
                        "" +
                        " FROM t_d_CivilPrice " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "CivilPriceId", "Name", "TaxRate", "Unit", "UnitPrice", "Memo" });
                SqlNum = "select count(CivilPriceId) as TotalNum FROM t_d_CivilPrice where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/CivilTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult CivilBriefView()
        {
            string CivilPriceId = Request.QueryString["CivilPriceId"].ToString();
            string sql = "select * from t_d_CivilPrice where CivilPriceId=" + CivilPriceId;
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            list = dao.GetList(sql, new string[] { "CivilPriceId", "Name", "TaxRate", "Unit", "UnitPrice", "Memo" });

            ViewBag.CivilPriceId = CivilPriceId;
            ViewBag.Name = list[0]["Name"].ToString();
            ViewBag.TaxRate = list[0]["TaxRate"].ToString();
            ViewBag.Unit = list[0]["Unit"].ToString();
            ViewBag.UnitPrice = list[0]["UnitPrice"].ToString();
            ViewBag.Memo = list[0]["Memo"].ToString();

            ViewBag.CivilPriceId = CivilPriceId;
            return View();

        }

        public ActionResult CivilCopy()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            string CivilPriceId = Request.Form["CivilPriceId"] == null ? null : Request.Form["CivilPriceId"].ToString().Replace(" ", "");
            string Name = Request.Form["Name"] == null ? null : Request.Form["Name"].ToString().Replace(" ", "");
            string LogTime = DateTime.Now.ToString();


            try
            {
                //复制主表
                string sql = "INSERT INTO t_d_CivilPrice (Name,TaxRate,Unit,UnitPrice,Memo,IsDelete,LogTime) " +
                    "SELECT '" + Name + "',TaxRate,Unit,UnitPrice,Memo,IsDelete,'" + LogTime + "' FROM t_d_CivilPrice WHERE CivilPriceId=" + CivilPriceId;
                BaseDao.execute(sql);
                //获取主表新增id
                sql = "select CivilPriceId from t_d_CivilPrice where LogTime='" + LogTime + "'";
                string NewCivilPriceId = dao.GetList(sql, new string[] { "CivilPriceId" })[0]["CivilPriceId"].ToString();
                //复制分表
                sql = "INSERT INTO t_d_CivilPriceSingle (XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,CivilPriceId,Memo)" +
                    " SELECT XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ," + NewCivilPriceId + ",Memo FROM t_d_CivilPriceSingle WHERE CivilPriceId=" + CivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult CivilExport()
        {

            string[] Ids = Request.Form.GetValues("CivilPriceId");

            ExcelTool export = new ExcelTool();
            XSSFWorkbook book = export.CivilPrice(Ids);
            var ms = new NpoiMemoryStream();
            ms.AllowClose = false;
            book.Write(ms);
            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "导出表格" + ".xlsx");

        }


        [HttpPost]
        public ActionResult AddCivilPrice()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string TaxRate = obj["TaxRate"] == null ? null : obj["TaxRate"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            if (TaxRate == "") TaxRate = "0.033";
            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["TaxRate"] = TaxRate,
                    ["Unit"] = Unit,
                    ["Memo"] = Memo,
                    ["LogTime"] = LogTime,
                    ["IsDelete"] = 0,
                };
                dao.save("t_d_CivilPrice", map);
                result.Add("result", "OK");
                string sql = "select * from t_d_CivilPrice where LogTime='" + LogTime + "'";
                string CivilPriceId = dao.GetList(sql, new string[] { "CivilPriceId" })[0]["CivilPriceId"].ToString();
                result.Add("CivilPriceId", CivilPriceId);
                result.Add("Name", Name);
                result.Add("TaxRate", TaxRate);
                result.Add("Unit", Unit);
                result.Add("Memo", Memo);


                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }


        public ActionResult CivilSingleTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,CivilPriceId,XMMC,GCLJSS,DW,GCL,RGDJ,CLDJ,Memo,Id" +
                        "" +
                        " FROM t_d_CivilPriceSingle " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "CivilPriceId", "XMMC", "GCLJSS", "DW", "GCL", "RGDJ", "CLDJ", "Memo","Id" });
                SqlNum = "select count(CivilPriceId) as TotalNum FROM t_d_CivilPriceSingle where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/CivilSingleTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult CivilPriceUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string CivilPriceId = obj["CivilPriceId"] == null ? null : obj["CivilPriceId"].ToString().Replace(" ", "");
            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string TaxRate = obj["TaxRate"] == null ? null : obj["TaxRate"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            if (TaxRate == "") TaxRate = "0.033";
            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["TaxRate"] = TaxRate,
                    ["Unit"] = Unit,
                    ["Memo"] = Memo,
                    
                };
                dao.update("t_d_CivilPrice", map, "CivilPriceId="+ CivilPriceId);

                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                string sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from t_d_CivilPriceSingle where CivilPriceId=" + CivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql = "select TaxRate from t_d_CivilPrice where CivilPriceId=" + CivilPriceId;
                string Tax = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(Tax);
                toltalnum = sumnum + sumnum * taxnum;
                sql = "update t_d_CivilPrice set UnitPrice=" + toltalnum.ToString("f2") + " where CivilPriceId=" + CivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult CivilPriceDel()
        {
            string CivilPriceId = Request.Form["CivilPriceId"] == null ? null : Request.Form["CivilPriceId"].ToString().Replace(" ", "");
            string sql = "update t_d_CivilPrice set IsDelete=1 where CivilPriceId=" + CivilPriceId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult CivilPriceSingleAdd()
        {

            Dictionary<string, object> result = new Dictionary<string, object>();

            string CivilPriceId = Request.Form["CivilPriceId"] == null ? null : Request.Form["CivilPriceId"].ToString().Replace(" ", "");
            string XMMC = Request.Form["XMMC"] == null ? null : Request.Form["XMMC"].ToString().Replace(" ", "");
            string GCLJSS = Request.Form["GCLJSS"] == null ? null : Request.Form["GCLJSS"].ToString().Replace(" ", "");
            string DW = Request.Form["DW"] == null ? null : Request.Form["DW"].ToString().Replace(" ", "");
            string GCL = Request.Form["GCL"] == null ? null : Request.Form["GCL"].ToString().Replace(" ", "");
            string RGDJ = Request.Form["RGDJ"] == null ? null : Request.Form["RGDJ"].ToString().Replace(" ", "");
            string CLDJ = Request.Form["CLDJ"] == null ? null : Request.Form["CLDJ"].ToString().Replace(" ", "");
            string Memo = Request.Form["Memo"] == null ? null : Request.Form["Memo"].ToString().Replace(" ", "");


            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["CivilPriceId"] = CivilPriceId,
                    ["XMMC"] = XMMC,
                    ["GCLJSS"] = GCLJSS,
                    ["DW"] = DW,
                    ["GCL"] = GCL,
                    ["RGDJ"] = RGDJ,
                    ["CLDJ"] = CLDJ,
                    ["Memo"] = Memo,
                };
                dao.save("t_d_CivilPriceSingle", map);
                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                string sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from t_d_CivilPriceSingle where CivilPriceId=" + CivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql= "select TaxRate from t_d_CivilPrice where CivilPriceId=" + CivilPriceId;
                string TaxRate = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(TaxRate);
                toltalnum = sumnum + sumnum * taxnum;
                sql= "update t_d_CivilPrice set UnitPrice="+toltalnum.ToString("f2")+ " where CivilPriceId=" + CivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult CivilPriceSingleUpdate()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            string CivilPriceId = Request.Form["CivilPriceId"] == null ? null : Request.Form["CivilPriceId"].ToString().Replace(" ", "");
            string Id = Request.Form["Id"] == null ? null : Request.Form["Id"].ToString().Replace(" ", "");
            string XMMC = Request.Form["XMMC"] == null ? null : Request.Form["XMMC"].ToString().Replace(" ", "");
            string GCLJSS = Request.Form["GCLJSS"] == null ? null : Request.Form["GCLJSS"].ToString().Replace(" ", "");
            string DW = Request.Form["DW"] == null ? null : Request.Form["DW"].ToString().Replace(" ", "");
            string GCL = Request.Form["GCL"] == null ? null : Request.Form["GCL"].ToString().Replace(" ", "");
            string RGDJ = Request.Form["RGDJ"] == null ? null : Request.Form["RGDJ"].ToString().Replace(" ", "");
            string CLDJ = Request.Form["CLDJ"] == null ? null : Request.Form["CLDJ"].ToString().Replace(" ", "");
            string Memo = Request.Form["Memo"] == null ? null : Request.Form["Memo"].ToString().Replace(" ", "");


            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["XMMC"] = XMMC,
                    ["GCLJSS"] = GCLJSS,
                    ["DW"] = DW,
                    ["GCL"] = GCL,
                    ["RGDJ"] = RGDJ,
                    ["CLDJ"] = CLDJ,
                    ["Memo"] = Memo,
                };
                dao.update("t_d_CivilPriceSingle", map,"Id="+Id);
                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                string sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from t_d_CivilPriceSingle where CivilPriceId=" + CivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql = "select TaxRate from t_d_CivilPrice where CivilPriceId=" + CivilPriceId;
                string TaxRate = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(TaxRate);
                toltalnum = sumnum + sumnum * taxnum;
                sql = "update t_d_CivilPrice set UnitPrice=" + toltalnum.ToString("f2") + " where CivilPriceId=" + CivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult CivilPriceSingleDel()
        {
            string CivilPriceId = Request.Form["CivilPriceId"] == null ? null : Request.Form["CivilPriceId"].ToString().Replace(" ", "");
            string Id = Request.Form["Id"] == null ? null : Request.Form["Id"].ToString().Replace(" ", "");
            string sql = "delete t_d_CivilPriceSingle where Id=" + Id;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                //下面计算总价
                double sumnum = 0;
                double taxnum = 0;
                double toltalnum = 0;
                sql = "select Round(SUM(GCL*(RGDJ+CLDJ)),2) as sumnum from t_d_CivilPriceSingle where CivilPriceId=" + CivilPriceId;
                string sum = dao.GetList(sql, new string[] { "sumnum" })[0]["sumnum"].ToString();
                if (sum == "")
                {
                    sumnum = 0;
                }
                else
                {
                    sumnum = double.Parse(sum);
                }
                sql = "select TaxRate from t_d_CivilPrice where CivilPriceId=" + CivilPriceId;
                string TaxRate = dao.GetList(sql, new string[] { "TaxRate" })[0]["TaxRate"].ToString();
                taxnum = double.Parse(TaxRate);
                toltalnum = sumnum + sumnum * taxnum;
                sql = "update t_d_CivilPrice set UnitPrice=" + toltalnum.ToString("f2") + " where CivilPriceId=" + CivilPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EquipPriceView()
        {
            return View();
        }

        public ActionResult EquipPriceSingleEditView()
        {
            string EquipPriceId = Request.QueryString["EquipPriceId"].ToString();
            string sql = "select * from t_d_EquipPrice where EquipPriceId=" + EquipPriceId;
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            list = dao.GetList(sql, new string[] { "EquipName", "Specification", "Unit", "Type","TypeL1", "NormalName", "NormalPrice", "NormalSpeci" });

            ViewBag.EquipPriceId = EquipPriceId;
            ViewBag.EquipName = list[0]["EquipName"].ToString();
            ViewBag.Specification = list[0]["Specification"].ToString();
            ViewBag.Unit = list[0]["Unit"].ToString();
            ViewBag.Type = list[0]["Type"].ToString();
            ViewBag.TypeL1 = list[0]["TypeL1"].ToString();
            ViewBag.NormalName = list[0]["NormalName"].ToString();
            ViewBag.NormalPrice = list[0]["NormalPrice"].ToString();
            ViewBag.NormalSpeci = list[0]["NormalSpeci"].ToString();

            return View();
        }

        public ActionResult EquipPriceDel()
        {
            string EquipPriceId = Request.Form["EquipPriceId"] == null ? null : Request.Form["EquipPriceId"].ToString().Replace(" ", "");
            string sql = "update t_d_EquipPrice set IsDelete=1 where EquipPriceId=" + EquipPriceId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EquipTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,Code,EquipPriceId,EquipName,Specification,Unit,Type,TypeL1,AvgPrice" +
                        "" +
                        " FROM t_d_EquipPrice " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "Code", "EquipPriceId", "EquipName", "Specification", "Unit", "Type", "TypeL1", "AvgPrice" });
                SqlNum = "select count(EquipPriceId) as TotalNum FROM t_d_EquipPrice where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/EquipSingleTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult EquipPriceAdd()
        {
            return View("EquipPriceSingleView");
        }


        public ActionResult EquipPriceFromAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string EquipName = obj["EquipName"] == null ? null : obj["EquipName"].ToString().Replace(" ", "");
            string Specification = obj["Specification"] == null ? null : obj["Specification"].ToString().Replace(" ", "");
            string Type = obj["Type"] == null ? null : obj["Type"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string Code = "";
            //下面根据类型生成编号
            int n = 0;
            string sql = "SELECT EquipPriceId FROM t_d_EquipPrice WHERE ";
            switch (Type)
            {
                case "设备":
                    sql += " code LIKE 'SB%'";
                    n = BaseDao.execute(sql);
                    n++;
                    Code = "SB" + n.ToString("00000");
                    break;

                case "电缆附件":
                    sql += " code LIKE 'FJ%'";
                    n = BaseDao.execute(sql);
                    n++;
                    Code = "FJ" + n.ToString("00000");
                    break; 
                case "电缆":
                    sql += " code LIKE 'DL%'";
                    n = BaseDao.execute(sql);
                    n++;
                    Code = "DL" + n.ToString("00000");
                    break;
                case "架空":
                    sql += " code LIKE 'JK%'";
                    n = BaseDao.execute(sql);
                    n++;
                    Code = "JK" + n.ToString("00000");
                    break;
                default:
                    break;
            }

            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["EquipName"] = EquipName,
                    ["Specification"] = Specification,
                    ["Type"] = Type,
                    ["Unit"] = Unit,
                    ["LogTime"] = LogTime,
                    ["Code"]=Code,
                    ["IsDelete"] = 0,
                };
                dao.save("t_d_EquipPrice", map);
                result.Add("result", "OK");
                sql = "select * from t_d_EquipPrice where LogTime='" + LogTime + "'";
                string EquipPriceId = dao.GetList(sql, new string[] { "EquipPriceId" })[0]["EquipPriceId"].ToString();
                result.Add("EquipPriceId", EquipPriceId);
                result.Add("EquipName", EquipName);
                result.Add("Specification", Specification);
                result.Add("Type", Type);
                result.Add("Unit", Unit);

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult EquipPriceFromUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string EquipPriceId = obj["EquipPriceId"] == null ? null : obj["EquipPriceId"].ToString().Replace(" ", "");
            string EquipName = obj["EquipName"] == null ? null : obj["EquipName"].ToString();
            string Specification = obj["Specification"] == null ? null : obj["Specification"].ToString();
            string Type = obj["Type"] == null ? null : obj["Type"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string NormalName = obj["NormalName"] == null ? null : obj["NormalName"].ToString();
            string NormalPrice = obj["NormalPrice"] == null ? null : obj["NormalPrice"].ToString().Replace(" ", "");
            string NormalSpeci = obj["NormalSpeci"] == null ? null : obj["NormalSpeci"].ToString();
            string TypeL1 = obj["TypeL1"] == null ? null : obj["TypeL1"].ToString();

            string LogTime = DateTime.Now.ToString();

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["EquipName"] = EquipName,
                    ["Specification"] = Specification,
                    ["Type"] = Type,
                    ["Unit"] = Unit,
                    ["IsDelete"] = 0,
                    ["NormalName"]= NormalName,
                    ["NormalPrice"]= NormalPrice,
                    ["NormalSpeci"]= NormalSpeci,
                    ["TypeL1"]= TypeL1,
                };
                dao.update("t_d_EquipPrice", map, "EquipPriceId="+ EquipPriceId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult EquipSingleTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,Id,SellerName,CONVERT(varchar(100),Buytime,23) as Buytime,Price,EquipPriceId,ProjectName" +
                        "" +
                        " FROM t_d_EquipPriceSingle " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "Id", "SellerName", "Buytime", "Price", "EquipPriceId", "ProjectName" });
                SqlNum = "select count(Id) as TotalNum FROM t_d_EquipPriceSingle where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/EquipSingleTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult EquipPriceFromSingleAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string EquipPriceId = obj["EquipPriceId"] == null ? null : obj["EquipPriceId"].ToString().Replace(" ", "");
            string SellerName = obj["SellerName"] == null ? null : obj["SellerName"].ToString().Replace(" ", "");
            string BuyTime = obj["BuyTime"] == null ? null : obj["BuyTime"].ToString().Replace(" ", "");
            string Price = obj["Price"] == null ? null : obj["Price"].ToString().Replace(" ", "");
            string ProjectName = obj["ProjectName"] == null ? null : obj["ProjectName"].ToString().Replace(" ", "");


            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["EquipPriceId"] = EquipPriceId,
                    ["SellerName"] = SellerName,
                    ["BuyTime"] = BuyTime,
                    ["Price"] = Price,
                    ["ProjectName"] = ProjectName,
                };
                dao.save("t_d_EquipPriceSingle", map);

                //下面要算个平均价格
                string sql = "SELECT round(AVG(price),2) as AvgPrice FROM t_d_EquipPriceSingle where EquipPriceId=" + EquipPriceId;
                string AvgPrice = dao.GetList(sql, new string[] { "AvgPrice" })[0]["AvgPrice"].ToString();
                if (string.IsNullOrEmpty(AvgPrice)) AvgPrice = "0";
                sql ="update t_d_EquipPrice set AvgPrice="+ AvgPrice+ " where EquipPriceId=" + EquipPriceId;
                BaseDao.execute(sql);
                result.Add("result", "OK");


                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult EquipPriceSingleDel()
        {
            string Id = Request.Form["Id"] == null ? null : Request.Form["Id"].ToString().Replace(" ", "");
            string EquipPriceId = Request.Form["EquipPriceId"] == null ? null : Request.Form["EquipPriceId"].ToString().Replace(" ", "");
            string sql = "delete t_d_EquipPriceSingle  where Id=" + Id;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                //下面要算个平均价格
                sql = "SELECT round(AVG(price),2) as AvgPrice FROM t_d_EquipPriceSingle where EquipPriceId=" + EquipPriceId;
                string AvgPrice = dao.GetList(sql, new string[] { "AvgPrice" })[0]["AvgPrice"].ToString();
                if (string.IsNullOrEmpty(AvgPrice)) AvgPrice = "0";
                sql = "update t_d_EquipPrice set AvgPrice=" + AvgPrice + " where EquipPriceId=" + EquipPriceId;
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult CoordFeeView()
        {
            return View("~/Views/PreEstimate/CoordFee/CoordFeeView.cshtml");
        }

        public ActionResult CoordFeeTableRequest()
        {

            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();


            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,CoordFeeId,CooContent,CooDepart,money,Memo" +
                        "" +
                        " FROM t_d_CoordFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "CoordFeeId", "CooContent", "CooDepart", "money", "Memo" });
                SqlNum = "select count(CoordFeeId) as TotalNum FROM t_d_CoordFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/CoordFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }


        public ActionResult CoordFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string CooContent = obj["CooContent"] == null ? null : obj["CooContent"].ToString().Replace(" ", "");
            string CooDepart = obj["CooDepart"] == null ? null : obj["CooDepart"].ToString().Replace(" ", "");
            string money = obj["money"] == null ? null : obj["money"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["CooContent"] = CooContent,
                    ["CooDepart"] = CooDepart,
                    ["money"] = money,
                    ["Memo"] = Memo,
                    ["IsDelete"] = 0,
                };
                dao.save("t_d_CoordFee", map);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult CoordFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string CoordFeeId = obj["CoordFeeId"] == null ? null : obj["CoordFeeId"].ToString().Replace(" ", "");
            string CooContent = obj["CooContent"] == null ? null : obj["CooContent"].ToString().Replace(" ", "");
            string CooDepart = obj["CooDepart"] == null ? null : obj["CooDepart"].ToString().Replace(" ", "");
            string money = obj["money"] == null ? null : obj["money"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["CooContent"] = CooContent,
                    ["CooDepart"] = CooDepart,
                    ["money"] = money,
                    ["Memo"] = Memo,
                    ["IsDelete"] = 0,
                };
                dao.update("t_d_CoordFee", map, "CoordFeeId=" + CoordFeeId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult CoordFeeDel()
        {
            string CoordFeeId = Request.Form["CoordFeeId"] == null ? null : Request.Form["CoordFeeId"].ToString().Replace(" ", "");
            string sql = "update t_d_CoordFee set IsDelete=1 where CoordFeeId=" + CoordFeeId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult TestFeeView()
        {
            return View("~/Views/PreEstimate/TestFee/TestFeeView.cshtml");
        }

        public ActionResult TestFeeTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,TestFeeId,TestName,money,Memo" +
                        "" +
                        " FROM t_d_TestFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "TestFeeId", "TestName", "money", "Memo" });
                SqlNum = "select count(TestFeeId) as TotalNum FROM t_d_TestFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/TestFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult TestFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string TestName = obj["TestName"] == null ? null : obj["TestName"].ToString().Replace(" ", "");
            string money = obj["money"] == null ? null : obj["money"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["TestName"] = TestName,
                    ["money"] = money,
                    ["Memo"] = Memo,
                    ["IsDelete"] = 0,
                };
                dao.save("t_d_TestFee", map);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult TestFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string TestFeeId = obj["TestFeeId"] == null ? null : obj["TestFeeId"].ToString().Replace(" ", "");
            string TestName = obj["TestName"] == null ? null : obj["TestName"].ToString().Replace(" ", "");
            string money = obj["money"] == null ? null : obj["money"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["TestName"] = TestName,
                    ["money"] = money,
                    ["Memo"] = Memo,
                    ["IsDelete"] = 0,
                };
                dao.update("t_d_TestFee", map, "TestFeeId=" + TestFeeId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult TestFeeDel()
        {
            string TestFeeId = Request.Form["TestFeeId"] == null ? null : Request.Form["TestFeeId"].ToString().Replace(" ", "");
            string sql = "update t_d_TestFee set IsDelete=1 where TestFeeId=" + TestFeeId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }


        public ActionResult InstallFeeView()
        {
            return View("~/Views/PreEstimate/InstallFee/InstallFeeView.cshtml");
        }

        public ActionResult InstallFeeTableRequest()
        {
            int page = Int32.Parse(Request.Form["page"]);
            int limit = Int32.Parse(Request.Form["limit"]);
            string condition = Request.Form["condition"].ToString();
            string SortName = Request.Form["SortName"].ToString();
            string Order = Request.Form["Order"].ToString();

            try
            {
                //以下是分页查询语句
                string sql = "", SqlNum = "";
                sql = " select top " + limit.ToString() + " * from " +
                        " ( " +
                        " SELECT ROW_NUMBER() OVER (order by " + SortName + " " + Order + ") as rowNumber,InstallFeeId,Name,Specification,Content,Unit,Money,Memo" +
                        "" +
                        " FROM t_d_InstallFee " +
                        " where 1=1 " + condition +
                        " ) as s " +
                        " where rowNumber>" + (limit * (page - 1)).ToString();

                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                list = dao.GetList(sql, new string[] { "InstallFeeId", "Name", "Specification", "Content", "Unit", "Money", "Memo" });
                SqlNum = "select count(InstallFeeId) as TotalNum FROM t_d_InstallFee where 1=1 " + condition;
                List<Dictionary<string, object>> ListNum = new List<Dictionary<string, object>>();
                ListNum = dao.GetList(SqlNum, new string[] { "TotalNum" });

                TableData td = new TableData();
                td.code = "";
                td.msg = "";
                td.count = int.Parse(ListNum[0]["TotalNum"].ToString());
                td.data = list;
                return Json(td);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> datalog = new Dictionary<string, object>();
                datalog.Add("type", int.Parse("1"));
                datalog.Add("OpTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                datalog.Add("OpID", int.Parse("1"));
                datalog.Add("OpLog", "异常：加载需求管理表格异常 /PreEstimateController/InstallFeeTableRequest:" + ex.Message.ToString().Replace("'", ""));
                dao.save("T_LOG", datalog);
                return Json('0');
            }

        }

        public ActionResult InstallFeeAdd()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string Specification = obj["Specification"] == null ? null : obj["Specification"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string Money = obj["Money"] == null ? null : obj["Money"].ToString().Replace(" ", "");
            string Content = obj["Content"] == null ? null : obj["Content"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            //Content = Content.Replace("\r\n", "\r\n<br>");
            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["Specification"] = Specification,
                    ["Unit"] = Unit,
                    ["Money"] = Money,
                    ["Name"] = Name,
                    ["Content"] = Content,
                    ["Memo"] = Memo,
                    ["IsDelete"] = 0,
                };
                dao.save("t_d_InstallFee", map);
                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

        public ActionResult InstallFeeUpdate()
        {
            var sr = new StreamReader(Request.InputStream);
            var stream = sr.ReadToEnd();
            JObject obj = JObject.Parse(stream);
            sr.Close();
            Dictionary<string, object> result = new Dictionary<string, object>();

            string InstallFeeId = obj["InstallFeeId"] == null ? null : obj["InstallFeeId"].ToString().Replace(" ", "");
            string Name = obj["Name"] == null ? null : obj["Name"].ToString().Replace(" ", "");
            string Specification = obj["Specification"] == null ? null : obj["Specification"].ToString().Replace(" ", "");
            string Unit = obj["Unit"] == null ? null : obj["Unit"].ToString().Replace(" ", "");
            string Money = obj["Money"] == null ? null : obj["Money"].ToString().Replace(" ", "");
            string Content = obj["Content"] == null ? null : obj["Content"].ToString().Replace(" ", "");
            string Memo = obj["Memo"] == null ? null : obj["Memo"].ToString().Replace(" ", "");

            try
            {
                Dictionary<string, object> map = new Dictionary<string, object>()
                {
                    ["Name"] = Name,
                    ["Specification"] = Specification,
                    ["Unit"] = Unit,
                    ["Money"] = Money,
                    ["Content"] = Content,
                    ["Name"] = Name,
                    ["Memo"] = Memo,
                };
                dao.update("t_d_InstallFee", map, "InstallFeeId=" + InstallFeeId);
                result.Add("result", "OK");

                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }

        }

        public ActionResult InstallFeeDel()
        {
            string InstallFeeId = Request.Form["InstallFeeId"] == null ? null : Request.Form["InstallFeeId"].ToString().Replace(" ", "");
            string sql = "update t_d_InstallFee set IsDelete=1 where InstallFeeId=" + InstallFeeId;

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                BaseDao.execute(sql);

                result.Add("result", "OK");
                return Json(result);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                result.Add("result", "error:" + e.Message.ToString().Replace("'", ""));
                return Json(result);
            }
        }

    }
}