﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YYNY.Tools
{
    /// <summary>
    /// 用于LAYUI数据表格
    /// </summary>
    public class TableData
    {
        public string code { set; get; }
        public string msg { set; get; }
        public int count { set; get; }
        public List<Dictionary<string, object>> data { set; get; }

    }
}